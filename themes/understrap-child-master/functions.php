<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();
    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery');
		wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), false);
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );
add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {

}

/* Redirect the user logging in to a custom admin page. */
function new_dashboard_home($username, $user){
    if(array_key_exists('administrator', $user->caps)){
        wp_redirect(admin_url('edit.php?post_type=shop_order', 'http'), 301);
        exit;
    }
}
add_action('wp_login', 'new_dashboard_home', 10, 2);

add_action( 'wp_network_dashboard_setup', function() {
    add_screen_option( 'layout_columns', array( 'default' => 2 ) );
} );


function my_login_logo() { ?>
    <style type="text/css">
       body.login.login-action-login.wp-core-ui.locale-en-us {
    background: url(https://dev.allo-sourcing.com/wp-content/uploads/2019/02/header2-1.jpg) no-repeat !important;
    background-size: cover !important;
}

       .login #backtoblog a, .login #nav a {
           background: #fff !important;
           display: block;
           margin: 0;
           padding: 10px;
       }

       .login #backtoblog, .login #nav{

           padding: 0 !important;
       }


    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


function my_custom_sidebar() {
		register_sidebar(
			array(
				'name' => __( 'Product characteristics', 'allo' ),
				'id'            => 'left-sidebar-1',
				'description' => __( 'Product characteristics', 'allo' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);
		register_sidebar(
			array (
					'name' => __( 'Supplier characteristics', 'allo' ),
					'id' => 'left-sidebar-2',
					'description' => __( 'Supplier characteristics', 'allo' ),
					'before_widget' => '<aside id="%1$s" class="widget %2$s">',
					'after_widget'  => '</aside>',
					'before_title' => '<h3 class="widget-title">',
					'after_title' => '</h3>',
			)
		);
		register_sidebar(
			array (
					'name' => __( 'Marking techniques', 'allo' ),
					'id' => 'left-sidebar-3',
					'description' => __( 'Marking techniques', 'allo' ),
					'before_widget' => '<aside id="%1$s" class="widget %2$s">',
					'after_widget'  => '</aside>',
					'before_title' => '<h3 class="widget-title">',
					'after_title' => '</h3>',
			)
		);
}
add_action( 'widgets_init', 'my_custom_sidebar' );

add_filter( 'manage_edit-shop_order_columns', 'misha_remove_woo_columns' );
function misha_remove_woo_columns( $cols ) {
    unset( $cols['order_total'] );
    return $cols;
}



remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );

/**
 * Rename product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

	$tabs['description']['title'] = __( 'Details' );		// Rename the description tab
	$tabs['reviews']['title'] = __( 'Customer reviews' );				// Rename the reviews tab
	$tabs['additional_information']['title'] = __( 'Product Data' );	// Rename the additional information tab

	return $tabs;

}
add_filter( 'woocommerce_endpoint_orders_title', 'change_my_account_orders_title', 10, 2 );
function change_my_account_orders_title( $title, $endpoint ) {
    $title = __( "Your orders", "woocommerce" );

    return $title;
}

/**
 * Remove product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;
}


// remove Order Notes from checkout field in Woocommerce
add_filter( 'woocommerce_checkout_fields' , 'alter_woocommerce_checkout_fields' );
function alter_woocommerce_checkout_fields( $fields ) {
    unset($fields['order']['order_comments']);
    return $fields;
}



/**
 * Add a custom product data tab
 */
add_filter( 'woocommerce_product_tabs', 'woo_new_tech_tab' );
function woo_new_tech_tab( $tabs ) {

	// Adds the new tab

	$tabs['tech_tab'] = array(
		'title' 	=> __( 'Technical Information', 'woocommerce' ),
		'priority' 	=> 15,
		'callback' 	=> 'woo_new_product_tab_technical'
	);
	$tabs['price_tab'] = array(
		'title' 	=> __( 'Pricing', 'woocommerce' ),
		'priority' 	=> 51,
		'callback' 	=> 'woo_new_product_tab_price'
	);

	$tabs['delivery_tab'] = array(
		'title' 	=> __( 'Delivery', 'woocommerce' ),
		'priority' 	=> 50,
		'callback' 	=> 'woo_new_product_tab_new_delivery'
	);

	return $tabs;

}

function woo_new_product_tab_technical() {


	return "test";


}

function woo_new_product_tab_price() {

	// The new tab content

	the_field('pricing');

}

function woo_new_product_tab_new_delivery() {

	// The new tab content

	the_field('delivery');

}


add_filter( 'add_to_cart_text', 'woo_custom_single_add_to_cart_text' );                // < 2.1
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_single_add_to_cart_text' );  // 2.1 +


function remove_linked_products($tabs){

    unset($tabs['linked_product']);
    unset($tabs['shipping']);
    unset($tabs['advanced']);
    unset($tabs['price']);

    return($tabs);

}

add_filter('woocommerce_product_data_tabs', 'remove_linked_products', 10, 1);


add_filter('woocommerce_template_loop_product_thumbnail', 'product_image_slider', 10, 1);

function product_image_slider() {

echo 'test';
}




add_filter( 'manage_edit-product_columns', 'change_columns_filter',10, 1 );
function change_columns_filter( $columns ) {
    unset($columns['price']);

    return $columns;
}




function woo_custom_single_add_to_cart_text() {

    return __( 'Ask for quote', 'woocommerce' );

}
add_action( 'init', 'default_woocommerce_settings');

function default_woocommerce_settings() {
  remove_filter( 'woocommerce_product_settings', 'x_woocommerce_remove_plugin_settings', 10 );
}
/////////////////////////////////////
add_action( 'woocommerce_before_add_to_cart_form', 'country_of_production', 5 );

function country_of_production() {
	echo '<div class="before-ask-for-quote"><p class="man-place"><strong>Manufacturing place: </strong>';
	echo the_field('country_of_production');
	echo '</p><div class="specify-the-delays"><button>specify the delays</button></div></div>';
}
//Customize container
add_action( 'woocommerce_before_add_to_cart_form', 'customize_container', 5 );

function customize_container() {
	echo '<div class="customize-container"></div>';
}

add_filter( 'woocommerce_order_button_text', 'woo_custom_order_button_text' );

function woo_custom_order_button_text() {
    return __( 'Ask for a quote', 'woocommerce' );
}
/**
  * Edit my account menu order
  */

 function my_account_menu_order() {
 	$menuOrder = array(
 		'dashboard'          => __( 'My account', 'woocommerce' ),
 		'quotes'             => __( 'Quotes checking', 'woocommerce' ),
		'previous-quotes'	=> __( 'Previous quotes', 'woocommerce'),
 		'edit-address'       => __( 'Addresses', 'woocommerce' ),
 		'edit-account'    	 => __( 'Account Details', 'woocommerce' ),
 		'customer-logout'    => __( 'Logout', 'woocommerce' ),
 	);
 	return $menuOrder;
 }
 add_filter ( 'woocommerce_account_menu_items', 'my_account_menu_order' );

 function add_previous_orders_endpoint() {
	 add_rewrite_endpoint( 'previous-quotes', EP_ROOT | EP_PAGES );
 }
add_action( 'init', 'add_previous_orders_endpoint' );

/**
 * Endpoints HTML content.
 */
function previous_orders() {
	get_template_part('woocommerce/myaccount/previous-orders');
}

add_action( 'woocommerce_account_previous-quotes_endpoint', 'previous_orders' );

/**
 * Change page titles
 */
add_filter( 'the_title', 'custom_account_endpoint_titles' );
function custom_account_endpoint_titles( $title ) {
    global $wp_query;

    if ( isset( $wp_query->query_vars['quotes'] ) && in_the_loop() ) {
        return 'Quotes checking';
    }

    if ( isset( $wp_query->query_vars['previous-quotes'] ) && in_the_loop() ) {
        return 'Previous quotes';
    }

    return $title;
}

// On cart page
add_action( 'woocommerce_cart_collaterals', 'remove_cart_totals', 9 );
function remove_cart_totals(){
    // Remove cart totals block
    remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10 );

    // Add back "Proceed to checkout" button (and hooks)
    echo '<div class="cart_totals">';
    do_action( 'woocommerce_before_cart_totals' );

    echo '<div class="wc-proceed-to-checkout">';
    do_action( 'woocommerce_proceed_to_checkout' );
    echo '</div>';

    do_action( 'woocommerce_after_cart_totals' );
    echo '</div><br clear="all">';
}

add_action( 'template_redirect', 'redirect_user_to_login_page' );

function redirect_user_to_login_page(){
    // Make sure your checkout page slug is correct
    if( is_page('cart') ) {
        if( !is_user_logged_in() ) {
            // Make sure your login page slug is correct in below line
            wp_redirect('/my-account/');
        }
    }
}

/**
 * Ensure cart contents update when products are added to the cart via AJAX
 */
function my_header_add_to_cart_fragment( $fragments ) {

    ob_start();
    $count = WC()->cart->cart_contents_count;
    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
      </a><?php

    $fragments['a.cart-contents'] = ob_get_clean();

    return $fragments;
}

add_filter( 'woocommerce_cart_needs_payment', '__return_false' );

function make_orders_editable( $is_editable, $order ) {
    // Allow only for admin and moderators
    if ( current_user_can( "manage_options" ) ) {
        return true;
    }
}
add_filter( 'wc_order_is_editable', 'make_orders_editable', 10, 2 );

add_action( 'add_meta_boxes', 'get_pdf' );
function get_pdf()
{
    add_meta_box( 'get_pdf', __( 'Get pdf' ), 'get_pdf_content', 'shop_order', 'normal', 'default' );
}
function get_pdf_content() {
    ?>
        <a id="get_pdf_details" href="javascript:void(0)" class="button"><?php _e('Get quote details'); ?></a>
				<script>
            $('#get_pdf_details').click(function(){

                var orderID = $('.woocommerce-order-data__heading').text();

                orderID = orderID.replace(/[^0-9.]/g, "");

                jQuery.post("../pdf.php", { data: orderID }, function () {
                    alert("The quotation was sucessfully created!");
                    window.open(
                      '/pdfs/'+orderID+'.pdf',
                      '_blank'
                    );
                }).fail(function () {
                    alert("Damn, something broke");
                });
            });
        </script>
    <?php
}

//add_action( 'add_meta_boxes', 'delays' );
function delays()
{
    add_meta_box( 'delays', __( 'Delay' ), 'delays_inputs', 'shop_order', 'normal', 'default' );
}
function delays_inputs() {
    ?>
		<div class="admin-delivery">
			<div class="admin-delivery-group">
				<label>Production:</label>
				<input type="number" id="delivery_production" name="delivery_production" placeholder="days" value="10"/>
			</div>
			<div class="admin-delivery-group">
				<label>Delivery:</label>
				<input type="number" id="delivery_delivery" name="delivery_delivery" placeholder="days" value="10"/>
			</div>
			<div class="admin-delivery-group">
				<label>BAT Mail:</label>
				<input type="text" id="delivery_mail" name="delivery_mail" value="10"/>
			</div>
			<div class="admin-delivery-group">
				<label>Prototype:</label>
				<input type="number" id="delivery_days" name="delivery_days" placeholder="days" value="10"/>
			</div>
		</div>
		<a id="set_delivery" href="javascript:void(0)" class="button"><?php _e('Set delays'); ?></a>
		<script>
		$(document).ready(function(){
								var orderID = $('.woocommerce-order-data__heading').text(),
							delivery_production = $('#delivery_production'),
							delivery_delivery = $('#delivery_delivery'),
							delivery_mail = $('#delivery_mail'),
							delivery_days = $('#delivery_days');

					orderID = orderID.replace(/[^0-9.]/g, "");

					jQuery.post("../getDelivery.php",
					{orderID},
					 function (result) {
							data = JSON.parse(result);
							delivery_production.val(data.production.meta_value);
							delivery_delivery.val(data.delivery.meta_value);
							delivery_mail.val(data.mail.meta_value);
							delivery_days.val(data.days.meta_value);
							console.log('Nice, all it is ok');
					}).fail(function () {
							console.log('Damn, something broke');
					});
				});
				$('#set_delivery').click(function(){

						var orderID = $('.woocommerce-order-data__heading').text(),
								delivery_production = $('#delivery_production').val(),
								delivery_delivery = $('#delivery_delivery').val(),
								delivery_mail = $('#delivery_mail').val(),
								delivery_days = $('#delivery_days').val();

						orderID = orderID.replace(/[^0-9.]/g, "");

						jQuery.post("../delivery.php",
						{
							orderID,
							delivery_production,
							delivery_delivery,
							delivery_mail,
							delivery_days
						}, function () {
								alert("The delivery was saved");
						}).fail(function () {
								alert("Damn, something broke");
						});
				});
		</script>
    <?php
}
// function remove_menus(){
// 		remove_menu_page( 'index.php' );
// 		remove_menu_page( 'edit.php' );
// 		remove_menu_page( 'edit.php?post_type=page' );
// 		remove_menu_page( 'tools.php' );
//     remove_menu_page( 'plugins.php' );
// }
// add_action( 'admin_menu', 'remove_menus', 999 );
//
// function remove_menus_custom_page(){
// 	remove_submenu_page( 'admin.php', 'berocket_account' );
// 	remove_menu_page( 'admin.php?page=yith_wcan_panel' );
// }
// add_action( 'admin_init', 'remove_menus_custom_page' );

// add_filter('site_url',  'wpadmin_filter', 10, 3);
//  function wpadmin_filter( $url, $path, $orig_scheme ) {
//   $old  = array( "/(wp-admin)/");
//   $admin_dir = WP_ADMIN_DIR;
//   $new  = array($admin_dir);
//   return preg_replace( $old, $new, $url, 1);
//  }


/*
 * Change the entry title of the endpoints that appear in My Account Page - WooCommerce 2.6
 * Using the_title filter
 */
function endpoint_title( $title, $id ) {
		global $wp;
  	$current_url = home_url( add_query_arg( array(), $wp->request ) );
		$url = parse_url($current_url);
		$array = explode('/', $url['path']);
		$endpoint = array_slice($array, 1);

    if ( $endpoint[1] === 'invoices-documents' && in_the_loop() ) {
        $title = "Invoices and documents";
    } elseif ( $endpoint[1] === 'previous-orders' && in_the_loop() ) {
				$title = "Previous orders";
		} elseif ( $endpoint[1] === 'orders' && in_the_loop() ) {
				$title = "Orders checking";
		}
    return $title;
}
add_filter( 'the_title', 'endpoint_title', 10, 2 );

add_action( 'admin_enqueue_scripts', 'load_admin_style' );
function load_admin_style() {
  wp_enqueue_style( 'admin-styles', get_stylesheet_directory_uri() . '/css/admin-style.css', array(), '1.0.0' );
}

wp_register_script(
  'admin-scripts',
  get_stylesheet_directory_uri() . '/js/admin-scripts.js',
  array(), '1.0.0', true
);

wp_enqueue_script('admin-scripts');
//$screen = get_current_screen();
//$title = get_admin_page_title();
$url_array = array(
	'templateUrl' => get_stylesheet_directory_uri(),
	'page' => $title
 );
//after wp_enqueue_script
wp_localize_script( 'admin-scripts', 'object', $url_array );


function custom_override_checkout_fields( $fields ) {
	unset( $fields['billing']['billing_first_name'] );
	unset( $fields['billing']['billing_last_name'] );
	unset( $fields['billing']['billing_phone'] );
	unset( $fields['billing']['billing_company'] );
	unset( $fields['billing']['billing_address_1'] );
	unset( $fields['billing']['billing_address_2'] );
	unset( $fields['billing']['billing_city'] );
	unset( $fields['billing']['billing_postcode'] );
	return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 20;
  return $cols;
}

add_action( 'woocommerce_after_single_product_summary', 'add_social', 9 );
function add_social() { ?>
	<div class="custom-social">
			<a href="http://www.facebook.com/share.php?u=<?php echo get_permalink(); ?>&title=<?php the_title(); ?>" target="_blank"> <i class="fab soc fa-facebook-square"></i></a>
			<a href="http://pinterest.com/pin/create/bookmarklet/?media=[MEDIA]&url=<?php the_title(); ?>&is_video=false&description=<?php the_title(); ?>" target="_blank"> <i class="fab soc fa-pinterest-p"></i></a>
			<a href="mailto:contact@allo-sourcing.com"> <i class="far soc fa-envelope" target="_blank"></i></a>
			<a href="http://twitter.com/intent/tweet?status=<?php echo get_permalink(); ?>" target="_blank"> <i class="fab soc fa-twitter"></i></a>
	</div>
<?php }

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'CTA',
		'menu_title'	=> 'CTA',
		'menu_slug' 	=> 'cta',
		'capability'	=> 'edit_posts'
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Exchange',
		'menu_title'	=> 'Exchange',
		'menu_slug' 	=> 'exchange',
		'capability'	=> 'edit_posts'
	));

}



add_action( 'woocommerce_single_product_summary', 'show_id_on_product', 5 );
function show_id_on_product(){
    global $product;
    echo '<div class="product_id">ID: ' . $product->get_id() . ' </div>';
}

// add_action( 'load-edit.php', 'posts_for_current_author' );
// function posts_for_current_author() {
//     global $user_ID;
// 		var_dump($user_ID);
//     /*if current user is an 'administrator' do nothing*/
//     //if ( current_user_can( 'add_users' ) ) return;
//
//     /*if current user is an 'administrator' or 'editor' do nothing*/
//     if ( current_user_can( 'edit_others_pages' ) ) return;
//
//     if ( ! isset( $_GET['author'] ) ) {
// 				//$wp_query->set( 'author', $user_ID);
//         //wp_redirect( add_query_arg( 'author', $user_ID ) );
// 				add_filter('views_edit-product', 'fix_post_counts');
//         exit;
//     }
//
// }

add_action( 'admin_head', 'custom_header');
function custom_header() {
	if ( is_admin() ) {
		global $current_user;
		$screen = get_current_screen();
		?>

		<?php
		if( $screen->base == 'post' ) {
			//wp_enqueue_script( 'jquery');
			?>
				<script>
					$(document).ready(function(){
						$('#postexcerpt').find('h2').after('<h2 class="custom-header">Short description</h2>');
						$('#postdivrich').prepend('<h2 class="hndle ui-sortable-handle"></h2><h2 class="custom-header">Details</h2>');
					});
				</script>
			<?php
		}
	}
}

add_action( 'admin_head', 'disable_admin_comment');
function disable_admin_comment() {
	if ( is_admin() ) {
		global $current_user;
		$screen = get_current_screen();
		?>

		<?php
		if( !current_user_can('manage_woocommerce') && $screen->base == 'post' ) {
			//wp_enqueue_script( 'jquery');
			?>
				<script>
					$(document).ready(function(){
						$('#admin-comment').find('textarea').attr('readonly', 'readonly');
					});
				</script>
			<?php
		}
	}
}

add_action( 'admin_head', 'hide_tablenav' );
function hide_tablenav() {
	if ( is_admin() ) {
		$screen = get_current_screen();

		if( $screen->base == 'users' ) {
		?>
				<script>
					$(document).ready(function(){
						console.log(1);
						$('.tablenav').hide();
					});
				</script>
			<?php
		}
	}
}

//Show only posts related to current user
add_action('pre_get_posts', 'query_set_only_author' );
function query_set_only_author( $wp_query ) {
		if( is_admin() ) {
			global $current_user;
			$screen = get_current_screen();

	    if( !current_user_can('manage_woocommerce') && $screen->base == 'edit' ) {
	        $wp_query->set( 'author', $current_user->ID );
	        add_filter('views_edit-product', 'fix_post_counts');
	    }
		}
}

// Fix post counts
function fix_post_counts($views) {
    global $current_user, $wp_query;
    unset($views['mine']);
    $types = array(
        array( 'status' =>  NULL ),
        array( 'status' => 'publish' ),
        array( 'status' => 'pending' ),
        array( 'status' => 'trash' ),
				array( 'status' => 'modification-needed' ),
				array( 'status' => 'not-selected' )
    );
    foreach( $types as $type ) {
        $query = array(
            'author'      => $current_user->ID,
            'post_type'   => 'product',
            'post_status' => $type['status']
        );
        $result = new WP_Query($query);
        if( $type['status'] == NULL ):
            $class = ($wp_query->query_vars['post_status'] == NULL) ? ' class="current"' : '';
            $views['all'] = sprintf(__('<a href="%s"'. $class .'>All <span class="count">(%d)</span></a>', 'all'),
                admin_url('edit.php?post_type=product'),
                $result->found_posts);
        elseif( $type['status'] == 'publish' ):
            $class = ($wp_query->query_vars['post_status'] == 'publish') ? ' class="current"' : '';
            $views['publish'] = sprintf(__('<a href="%s"'. $class .'>Published <span class="count">(%d)</span></a>', 'publish'),
                admin_url('edit.php?post_status=publish&post_type=product'),
                $result->found_posts);
        elseif( $type['status'] == 'draft' ):
            $class = ($wp_query->query_vars['post_status'] == 'draft') ? ' class="current"' : '';
            $views['draft'] = sprintf(__('<a href="%s"'. $class .'>Draft'. ((sizeof($result->posts) > 1) ? "s" : "") .' <span class="count">(%d)</span></a>', 'draft'),
                admin_url('edit.php?post_status=draft&post_type=product'),
                $result->found_posts);
        elseif( $type['status'] == 'pending' ):
            $class = ($wp_query->query_vars['post_status'] == 'pending') ? ' class="current"' : '';
            $views['pending'] = sprintf(__('<a href="%s"'. $class .'>Pending <span class="count">(%d)</span></a>', 'pending'),
                admin_url('edit.php?post_status=pending&post_type=product'),
                $result->found_posts);
        elseif( $type['status'] == 'trash' ):
            $class = ($wp_query->query_vars['post_status'] == 'trash') ? ' class="current"' : '';
            $views['trash'] = sprintf(__('<a href="%s"'. $class .'>Trash <span class="count">(%d)</span></a>', 'trash'),
                admin_url('edit.php?post_status=trash&post_type=product'),
                $result->found_posts);
				elseif( $type['status'] == 'modification-needed' ):
            $class = ($wp_query->query_vars['post_status'] == 'modification-needed') ? ' class="current"' : '';
            $views['modification-needed'] = sprintf(__('<a href="%s"'. $class .'>Modification needed <span class="count">(%d)</span></a>', 'pending'),
                admin_url('edit.php?post_status=modification-needed&post_type=product'),
                $result->found_posts);
					elseif( $type['status'] == 'not-selected' ):
	            $class = ($wp_query->query_vars['post_status'] == 'not-selected') ? ' class="current"' : '';
	            $views['not-selected'] = sprintf(__('<a href="%s"'. $class .'>Not selected <span class="count">(%d)</span></a>', 'pending'),
	                admin_url('edit.php?post_status=not-selected&post_type=product'),
	                $result->found_posts);
        endif;
    }
    return $views;
}

add_action( 'admin_menu', 'my_admin_menu' );

function my_admin_menu() {
	global $current_user;

	if ( current_user_can('manage_woocommerce') ) {
			add_menu_page( 'Supplier list', 'Supplier', 'manage_options', 'supplaiers/suppliers.php', 'supplier_menu' );
	}
}

function supplier_menu() {
	$args = array(
	//'blog_id'      => $GLOBALS['blog_id'],
	//'role'         => array('supplier','administrator'),
	'role__in'     => array('supplier','block'),
	//'role__not_in' => array(),
	//'meta_key'     => '',
	//'meta_value'   => '',
	//'meta_compare' => '',
	//'meta_query'   => array(),
	//'date_query'   => array(),
	//'include'      => array(),
	//'exclude'      => array(),
	'orderby'      => 'login',
	'order'        => 'ASC'
	//'offset'       => '',
	//'search'       => '',
	//'number'       => '',
	//'count_total'  => false,
	//'fields'       => 'all',
	//'who'          => '',
 );
 $suppliers = get_users( $args );
	?>
	<div class="wrap">
		<h2>Suppliers</h2>
			<div class="suppliers">
				<?php foreach ( $suppliers as $key=>$supplier ) {
					$supplierData = get_userdata($supplier->ID);
					$block = (implode(', ', $supplierData->roles) === 'block') ? '<span class="supplier-block">(block)</span>' : '';
					?>
					<div class="supplier" data-user-id="<?php echo $supplier->ID; ?>" data-products="false" data-role="<?php echo implode(', ', $supplierData->roles); ?>">
						<?php
						echo '<div class="supplier-number">' . ($key+1) . '</div>';
						echo '<div class="supplier-name">' . $supplier->user_login . ' ' . $block . '</div>';
						echo '<div class="supplier-undo"><i class="fas fa-undo"></i></div>';
						echo '<div class="supplier-trash"><i class="far fa-trash-alt"></i></div>';
						$args = array(
					    'author'        =>  $supplier->ID,
							'post_status'		=> 	array('publish', 'pending', 'modification-needed', 'trash', 'not-selected'),
					    'orderby'       =>  'post_date',
							'post_type'     =>  'product'
					    );
						$products = get_posts( $args );
						//var_dump($products);?>
					</div>
					<div id="supplier-products-<?php echo $supplier->ID; ?>" class="supplier-products"></div>
				<?php } ?>
			</div>
	</div>
	<?php
}
add_action('admin_head', 'hide_change_status_for_supplier');
function hide_change_status_for_supplier(){
	global $current_user;
	if ( current_user_can( 'supplier' ) )
    echo "<style>
			#minor-publishing-actions, #post-status-select, .misc-pub-visibility, #toplevel_page_pp-manage-roles {display:none !important;}
		</style>";

}
