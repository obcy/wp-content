<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
global $post;

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta property="og:url"                content="<?php the_permalink(); ?>" />
	<meta property="og:type"               content="article" />
	<meta property="og:title"              content="<?php the_title();?>" />
	<meta property="og:description"        content="<?php echo $post->post_excerpt; ?>" />
 	<meta property="og:image"              content="<?php echo get_the_post_thumbnail_url();?>" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="/wp-content/themes/understrap-child-master/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <link rel="stylesheet" href="http://dev.allo-sourcing.com/assets/slick/slick-theme.css">

	    <!-- Style sheets -->
    <link rel="stylesheet" type="text/css" href="http://dev.allo-sourcing.com/assets/css/main.css">

    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/owl.theme.default.css">
    <!-- The CSS for the plugin itself - required -->
    <link rel="stylesheet" type="text/css" href="http://dev.allo-sourcing.com/assets/css/FancyProductDesigner-all.min.css" />

    <script src="http://dev.allo-sourcing.com/assets/js/jquery.min.js" type="text/javascript"></script>
    <script src="http://dev.allo-sourcing.com/assets/js/jquery-ui.min.js" type="text/javascript"></script>

		<!-- Drift chat -->
		<!-- Start of Async Drift Code -->
<script>
"use strict";

!function() {
  var t = window.driftt = window.drift = window.driftt || [];
  if (!t.init) {
    if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
    t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ],
    t.factory = function(e) {
      return function() {
        var n = Array.prototype.slice.call(arguments);
        return n.unshift(e), t.push(n), t;
      };
    }, t.methods.forEach(function(e) {
      t[e] = t.factory(e);
    }), t.load = function(t) {
      var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
      o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
      var i = document.getElementsByTagName("script")[0];
      i.parentNode.insertBefore(o, i);
    };
  }
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('dvidkbbfdk68');
</script>
<!-- End of Async Drift Code -->
</head>

<body <?php body_class(); ?>>

<div class="site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<header>
	<div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-12">
                        <p>Allo-sourcing the partner for your custom made project</p>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="contact-info">
                            <a href="tel:00330478875555"> + 33 (0)4 78 87 55 55</a> - 
                            <a href="mailto:contact@allo-sourcing.com">contact@allo-sourcing.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-12">
                        <div class="logo text-center" >
                            <a href="index.html">
                                <?php the_custom_logo() ?>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="search-box">
														<?php get_search_form(); ?>
                        </div>
                    </div>
                    <div class="col-lg-3 col-12">
											<div class="user-panel pt-4">
												<?php get_template_part( 'template-parts/user', 'interface' ); ?>
                        <?php get_template_part( 'template-parts/user', 'language' ); ?>
											</div>
											<div class="user-underpanel">
												<?php get_template_part( 'template-parts/user', 'wishlist' ); ?>
												<?php get_template_part( 'template-parts/user', 'cart' ); ?>
											</div>

                    </div>

                </div>
            </div>
        </div>
     <div class="mobile-menu" style="text-align: right;">
         <h3>MENU</h3>
        <img src="../../../../assets/img/menu.png" style=" max-width: 35px; height: 35px;" />

     </div>
				<?php get_template_part( 'template-parts/menu' ); ?>
	</header>
