<?php
/**
* Template Name: Our skills
*/

get_header();

$tiles = get_field('our_skills');
?>
<sections id="our-skills">
  <div class="our-skills-title">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <h1><?php the_title(); ?></h1>
          <?php while ( have_posts() ) : the_post();?>
            <p><?php the_content(); ?></p>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="our-skills-tiles">
    <div class="container">
      <div class="row justify-content-center">
        <?php foreach ($tiles as $tile) : ?>
          <div class="col-sm-12 col-md-6 col-lg-4 tile">
            <div class="icon" style="background: <?php echo $tile['color']; ?>;">
              <img src="<?php echo $tile['icon']['url']; ?>" alt="<?php echo $tile['icon']['title']; ?>"/>
            </div>
            <div class="description">
              <h2 style="color: <?php echo $tile['color']; ?>;"><?php echo $tile['title']; ?></h2>
              <p><?php echo $tile['content']; ?></p>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</section>
<?php get_template_part( 'template-parts/icons' ); ?>
<?php get_footer(); ?>
