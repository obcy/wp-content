<?php
/**
* Template Name: Maquette
*/

get_header();

?>

<div class="container">
<div class="row text-center maquette my-5 py-3">

    <?php

    // check if the repeater field has rows of data
    if( have_rows('maquette') ):
        $count = 0;
        // loop through the rows of data
        while ( have_rows('maquette') ) : the_row();

            // display a sub field value

          ?>

        <?php  if ($count  == 4) {
      $group++;
      ?>
                <div class="col-md-4 col-sm-12 my-5">
                  <div class="square">
                    <h4>IN A FEW <br/> FIGURES</h4>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 my-3">
                    <img src="<?php  the_sub_field('icon'); ?>" style="width: 130px;" /> <br/>
                    <h2><?php  the_sub_field('header'); ?></h2>
                    <h3><?php  the_sub_field('subtitle'); ?></h3>
                    <h5><?php  the_sub_field('description'); ?></h5>
                </div>
      <?php
    } else {?>

            <div class="col-md-4 col-sm-6  my-3">
                <img src="<?php  the_sub_field('icon'); ?>" style="width: 130px;" /> <br/>
                <h2><?php  the_sub_field('header'); ?></h2>
                <h3><?php  the_sub_field('subtitle'); ?></h3>
                <h5><?php  the_sub_field('description'); ?></h5>
            </div>

       <?php } ?>


    <?php


       $count++;
        endwhile;

    else :

        // no rows found

    endif;

    ?>

</div>
</div>



<?php get_template_part( 'template-parts/cta' ); ?>
<?php get_footer(); ?>
