$(document).ready(function(){
  var buttonPublish = $('#publish'),
      checkboxCustomization = $('.acf-field-true-false input[type="checkbox"]'),
      priceRangeUsd = $('#price_range_usd input'),
      usd = $('#usd input'),
      data = [];
  buttonPublish.click(function(){
    if(checkboxCustomization.attr('checked')) {
      var customizeTitles = $('.customize-title input'),
          custoomizeImages = $('.customize-image img'),
          postID = $('#post_ID').val();
      for(var i = 0; i < customizeTitles.length - 1; i++) {
        if (i == 0) {
          data[i] = {
            postID:postID,
            title:$(customizeTitles[i]).val(),
            image:$(custoomizeImages[i]).attr('src')
          }
        } else {
          data[i] = {
              title:$(customizeTitles[i]).val(),
              image:$(custoomizeImages[i]).attr('src')
            }
        }
      }
      jQuery.post("../customization.php",
      {
        data
      }, function () {
          console.log("Data send");
      }).fail(function () {
          console.log("Damn, something broke");
      });
    }
    if(priceRangeUsd.length != 0) {
      console.log('product USD');
      var postID = $('#post_ID').val(),
          rangeEuro = $('div[data-name="price_range"] input').val(),
          rangeUSD = $('#price_range_usd input');
      jQuery.post("../exchangeSingleProduct.php",
      {
        rangeEuro,
        postID
      }, function (result) {
          data = JSON.parse(result);
          console.log("Product USD update");
          rangeUSD.val(data.range_usd);
      }).fail(function () {
          console.log("Damn, something broke");
      });
    }
    if(usd.length != 0) {
      console.log('USD');
      var usdVal = $(usd).val();
      jQuery.post("../exchange.php",
      {
        usdVal
      }, function () {
          console.log("Global USD send");
      }).fail(function () {
          console.log("Damn, something broke");
      });
    }
  });
  $( 'input[value="Image"]' ).parent().parent().hide();
  $( 'input[value="images_quantity"]' ).parent().parent().hide();
  $( 'input' ).filter(function(){ return this.value.match(/^image_.+$/) }).parent().parent().hide();
  $( 'input[value="Custom-description"]' ).attr('readonly', 'readonly');
  $( 'input[value="Color"]' ).attr('readonly', 'readonly');
  $( 'input[value="Current-currency"]' ).attr('readonly', 'readonly');
  $( 'input[value="Price-range"]' ).attr('readonly', 'readonly');
  $( 'input[value="Customer-quantity"]' ).attr('readonly', 'readonly');
  $( 'input[value="Custom-description"]' ).attr('readonly', 'readonly');
  $( 'input[value="Production"]' ).attr('readonly', 'readonly');
  $( 'input[value="Delivery"]' ).attr('readonly', 'readonly');
  $( 'input[value="BAT-Mail"]' ).attr('readonly', 'readonly');
  $( 'input[value="Prototype"]' ).attr('readonly', 'readonly');
  $( 'input[value="Quantity-1"]' ).attr('readonly', 'readonly');
  $( 'input[value="Quantity-2"]' ).attr('readonly', 'readonly');
  $( 'input[value="Quantity-3"]' ).attr('readonly', 'readonly');
  $( 'input[value="Option-1"]' ).attr('readonly', 'readonly');
  $( 'input[value="Option-2"]' ).attr('readonly', 'readonly');
  $( 'input[value="Option-3"]' ).attr('readonly', 'readonly');
  $( 'input[value="Size-S"]' ).attr('readonly', 'readonly');
  $( 'input[value="Size-M"]' ).attr('readonly', 'readonly');
  $( 'input[value="Size-L"]' ).attr('readonly', 'readonly');
  $( 'input[value="Size-XL"]' ).attr('readonly', 'readonly');
  $( 'input[value="Custom"]' ).attr('readonly', 'readonly');
  $( 'input[value="Custom-comment"]' ).attr('readonly', 'readonly');
  $( 'input[value="Options"]' ).attr('readonly', 'readonly');
  $( 'input[value="Marks"]' ).attr('readonly', 'readonly');
  $( 'input[value="Length"]' ).attr('readonly', 'readonly');
  $( 'input[value="Width"]' ).attr('readonly', 'readonly');
  $( 'input[value="Height"]' ).attr('readonly', 'readonly');
});

jQuery(document).ready(function($){
  var mediaUploader,
      item,
      imagesLimit = 3;
  ($('.item .wc-order-item-images').attr('data-images-quantity') == 3) && $(this).find('.wc-order-item-add').hide();
  $('.wc-order-item-add').click(function(e) {
    item = $(this).parents('.item');
    e.preventDefault();
      if (mediaUploader) {
      mediaUploader.open();
      return;
    }
    mediaUploader = wp.media.frames.file_frame = wp.media({
      title: 'Add Custom Image',
      button: {
      text: 'Choose Image'
    }, multiple: false });
    mediaUploader.on('select', function() {
      var attachment = mediaUploader.state().get('selection').first().toJSON(),
          images = $(item).find('.wc-order-item-images'),
          imagesQuantity = parseInt($(images).attr('data-images-quantity')),
          orderItemId = $(item).attr('data-order_item_id'),
          imageName = 'image_' + orderItemId + '_' + id();
      var template = `
          <div class="wc-order-item-image" data-image-id="`+ imageName +`">
            <img src="`+ attachment.url +`" alt="`+ imageName +`"/>
            <div class="wc-order-item-delete button">x</div>
          </div>
          `;
          $(images).prepend(template);
          imagesQuantity = imagesQuantity + 1;
          $(images).attr('data-images-quantity', imagesQuantity);
          (imagesQuantity === imagesLimit) && $(images).find('.wc-order-item-add').hide();
          setImagesQuantity(imagesQuantity, orderItemId);
          saveImage(orderItemId, attachment.url, imageName);
    });
    mediaUploader.open();
  });
  $('.wc-order-item-images').on('click', '.wc-order-item-delete', function(){
    var item = $(this).parents('.item'),
        images = $(item).find('.wc-order-item-images'),
        imagesQuantity = parseInt($(images).attr('data-images-quantity')),
        orderItemId = $(item).attr('data-order_item_id'),
        imageName;
        imageName = $(this).parents('.wc-order-item-image').attr('data-image-id');
        console.log(this);
        console.log(item);
        console.log(imageName);
    $(this).parent().remove();
    deleteImage(orderItemId, imageName);
    imagesQuantity = imagesQuantity - 1;
    $(images).attr('data-images-quantity', imagesQuantity);
    setImagesQuantity(imagesQuantity, orderItemId);
    (imagesQuantity < imagesLimit) && $(item).find('.wc-order-item-add').show();
  });
  function id() {
    return Math.random().toString(36).substr(2, 9) + '_' + Math.random().toString(36).substr(2, 9);
  };
  function setImagesQuantity(imagesQuantity, orderItemId) {
    $.ajax({
      type: "POST",
      url: '../quantityImages.php',
      data: {
        imagesQuantity: imagesQuantity,
        orderItemId: orderItemId
      },
      success: function() {
        console.log('Set quantity');
      }
    });
  }
  function saveImage(orderItemId, imageUrl, imageName) {
    $.ajax({
      type: "POST",
      url: '../saveImage.php',
      data: {
        orderItemId: orderItemId,
        imageUrl: imageUrl,
        imageName: imageName
      },
      success: function(data) {
        console.log('Save image');
      }
    });
  }
  function deleteImage(orderItemId, imageName) {
    $.ajax({
      type: "POST",
      url: '../deleteImage.php',
      data: {
        orderItemId: orderItemId,
        imageName: imageName,
        status: true
      },
      success: function() {
        console.log('Delete image');
      }
    });
  }
  $('h2:contains("About the user")').hide();
  $('h2:contains("Editorial Notifications")').hide();
  $('.user-description-wrap').hide();
  $('.user-profile-picture').hide();
  $('.psppno_workflow_user_fields').hide();

  $("#acf-group_5d14b3eb65cc9").appendTo("#postbox-container-1");

  function getProducts(supplier, suppliersProducts, supplierID, supplierProducts) {
    console.log(supplier);
    console.log(suppliersProducts);
    console.log(supplierID);
    console.log(supplierProducts);
    if (suppliersProducts == 'false') {
      $.ajax({
        type: "GET",
        url: '../supplierProducts.php',
        data: {
          supplierID: supplierID
        },
        success: function(res) {
          supplierProducts.html(res);
          console.log(res);
          console.log('Get products.');
        },
        complete: function() {
          supplier.attr('data-products', 'true');
        }
      });
    }
  }

  $('.supplier-name').click(function(){
    var supplier = $(this).parent(),
        suppliersProducts = $(this).parent().attr('data-products'),
        supplierID = $(this).parent().attr('data-user-id'),
        supplierProducts = $('.suppliers').find('#supplier-products-' + supplierID);
    getProducts(supplier, suppliersProducts, supplierID, supplierProducts);
    supplierProducts.toggle();
  });

  $('.supplier-undo').click(function(){
    var supplier = $(this).parent(),
        suppliersProducts = $(this).parent().attr('data-products'),
        supplierID = $(this).parent().attr('data-user-id'),
        supplierProducts = $('.suppliers').find('#supplier-products-' + supplierID);
        if (suppliersProducts == 'true') {
            supplier.attr('data-products', 'false');
        }
        suppliersProducts = $(this).parent().attr('data-products');
    $.ajax({
      type: "GET",
      url: '../supplierRestore.php',
      data: {
        supplierID: supplierID
      },
      success: function() {
        supplier.attr('data-role', 'supplier');
        if(supplier.attr('data-role') === 'supplier') {
          console.log(1);
          supplier.find('.supplier-name').find('.supplier-block').remove();
        }
        console.log('Supplier restore.');
      },
      complete: function() {
        getProducts(supplier, suppliersProducts, supplierID, supplierProducts);
      }
    });
  });

  $('.supplier-trash').click(function(){
    var supplier = $(this).parent(),
        suppliersProducts = $(this).parent().attr('data-products'),
        supplierID = $(this).parent().attr('data-user-id'),
        supplierProducts = $('.suppliers').find('#supplier-products-' + supplierID);
        if (suppliersProducts == 'true') {
            supplier.attr('data-products', 'false');
        }
        suppliersProducts = $(this).parent().attr('data-products');
      $.ajax({
        type: "GET",
        url: '../supplierTrash.php',
        data: {
          supplierID: supplierID
        },
        success: function() {
          supplier.attr('data-role', 'block');
          if(supplier.attr('data-role') === 'block') {
            console.log(2);
            supplier.find('.supplier-name').append('<span class="supplier-block">(block)</span>');
          }
          console.log('Supplier move to trash.');
        },
        complete: function() {
          getProducts(supplier, suppliersProducts, supplierID, supplierProducts);
        }
      });
  });
});
