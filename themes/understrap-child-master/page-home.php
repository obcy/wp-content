<?php
/**
* Template Name: Home page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header();
?>

<div class="owl-carousel">

     <?php

                                // check if the repeater field has rows of data
                                if( have_rows('owl-slider') ):

                                    // loop through the rows of data
                                    while ( have_rows('owl-slider') ) : the_row();

       ?>

    <div>  <section id="main-slider" style="background: url(<?php the_sub_field('banner_background') ?>)">
            <div class=" ">
                <div class="row">
                    <div class="col-md-8 col-12">
                        <div id="slider-box">
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="info-box">
                            <h1><?php the_sub_field('banner_title') ?></h1>
                            <p><?php the_sub_field('banner_text') ?></p>
                            <ul>
                                <?php

                                // check if the repeater field has rows of data
                                if( have_rows('banner_list') ):

                                    // loop through the rows of data
                                    while ( have_rows('banner_list') ) : the_row();

                                        // display a sub field value

                                        echo '<li class="asdasd">';

                                        ?>
                                        <img src="<?php the_sub_field('list_image')?>" />
                                        <?php

                                        the_sub_field('list_text');
                                        echo '</li>';

                                    endwhile;

                                else :

                                    // no rows found

                                endif;

                                ?>
                            </ul>
                            <a href="<?php the_sub_field('banner_button_link') ?>">
                                <button class="btn"><?php the_sub_field('banner_button_text') ?></button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php
    endwhile;

    else :

    // no rows found

    endif;

    ?>
</div>

<div class="csstransforms">
  <div class="flash-info">
   <h3>FLASH INFO</h3>
      <h4><?php the_field('header_flash'); ?></h4>
      <p><?php the_field('description_flash'); ?></p>
      <button>read more</button>
  </div>
</div>



      <?php get_template_part( 'template-parts/advices' ); ?>
      <?php get_template_part( 'template-parts/icons' ); ?>
      <section id="banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-5 image" style="background-image: url(<?php the_field('sec_banner_background') ?>);">
                    <div class="image-content">
                        <h3>
                            <?php the_field('sec_banner_title') ?>
                        </h3>
                        <button class="btn white-bg blue-color">
                          <?php
                              $cat = get_field('sec_button_url');
                              $link = get_term_link( $cat, 'product_cat' );
                           ?>
                           <a href="<?php echo $link; ?>"><?php the_field('sec_button_text') ?></a>
                        </button>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-7">
                  <div class="products">
                      <div class="row events">
                        <?php
                            $args = array(
                              'post_type' => 'product',
                              'posts_per_page' => 6,
                              'orderby' => 'rand',
                              'tax_query' => array(
                                   array(
                                        'taxonomy' => 'product_cat',
                                        'field' => 'id',
                            						'terms' => $cat
                                    ),
                                ),
                              );

                              $loop = new WP_Query( $args );

                              if ( $loop->have_posts() ) {
                        				if ( $loop->have_posts() ) {
                        					while ( $loop->have_posts() ) : $loop->the_post();
                        						wc_get_template_part( 'content', 'product' );
                        					endwhile;
                        				} else {
                        					echo __( 'No products found' );
                        				}
                        			}
                              wp_reset_postdata();
                        ?>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </section>
    <section class="blurbs">
        <div class="container fullwidth">
            <div class="row">
                <div class="col-md-4 col-xl-2 col-12">
                    <div class="blurb-box">
                        <div class="blurb-img">
                            <img src="assets/img/icones/consulting.png" alt="" class="src">
                        </div>
                        <div class="blurb-desc">
                            <p class="blurb-title"><?php the_field('icon1_title')?></p>
                            <p class="blurb-text"><?php the_field('icon#1_text')?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xl-2 col-12">
                    <div class="blurb-box">
                        <div class="blurb-img">
                            <img src="assets/img/icones/design.png" alt="" class="src">
                        </div>
                        <div class="blurb-desc">
                            <p class="blurb-title"><?php the_field('icon2_title')?></p>
                            <p class="blurb-text"><?php the_field('icon#2_text')?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xl-2 col-12">
                    <div class="blurb-box">
                        <div class="blurb-img">
                            <img src="assets/img/icones/quality.png" alt="" class="src">
                        </div>
                        <div class="blurb-desc">
                            <p class="blurb-title"><?php the_field('icon3_title')?></p>
                            <p class="blurb-text"><?php the_field('icon#3_text')?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xl-2 col-12">
                    <div class="blurb-box">
                        <div class="blurb-img">
                            <img src="assets/img/icones/sourcing.png" alt="" class="src">
                        </div>
                        <div class="blurb-desc">
                            <p class="blurb-title"><?php the_field('icon4_title')?></p>
                            <p class="blurb-text"><?php the_field('icon#4_text')?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xl-2 col-12">
                    <div class="blurb-box">
                        <div class="blurb-img">
                            <img src="assets/img/icones/delivery.png" alt="" class="src">
                        </div>
                        <div class="blurb-desc">
                            <p class="blurb-title"><?php the_field('icon5_title')?></p>
                            <p class="blurb-text"><?php the_field('icon#5_text')?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xl-2 col-12">
                    <div class="blurb-box">
                        <div class="blurb-img">
                            <img src="assets/img/icones/deadline.png" alt="" class="src">
                        </div>
                        <div class="blurb-desc">
                            <p class="blurb-title"><?php the_field('icon6_title')?></p>
                            <p class="blurb-text"><?php the_field('icon#6_text')?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="videos">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h2 class="title">OUR INNOVATIONS IN VIDEOS</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-12">
                    <div class="video1" id="video1">
                        <div class="video-box">
                            <?php the_field('video_1_url'); ?>
                        </div>
                        <h4 class="video-title">
                            <?php the_field('video_1_title') ?>
                        </h4>
                    </div>

                </div>
                <div class="col-md-4 col-12">
                    <div class="video2" id="video2">
                        <div class="video-box">
                            <?php the_field('video_2_url'); ?>
                        </div>
                        <h4 class="video-title">
                            <?php the_field('video_2_title') ?>
                        </h4>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="video3" id="video3">
                        <div class="video-box">
                            <?php the_field('video_3_url'); ?>
                        </div>
                        <h4 class="video-title">
                            <?php the_field('video_3_title') ?>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php get_template_part( 'template-parts/cta' ); ?>
<?php get_footer(); ?>
