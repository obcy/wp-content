<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>


	<footer>
		<?php // var_dump(get_defined_vars()); ?>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-12">
                    <img src="http://dev.allo-sourcing.com/assets/img/logo-blanc.png" alt="Global products">
                </div>
                <div class="col-md-4 col-12">
                    <h4>Custom Manufacturing Specialist</h4>
                    <ul>
                        <li>Our marking techniques</li>
                        <li>All our ranges</li>
                        <li>Manufactured in Europe</li>
                        <li>Our engagements</li>
                        <li>Our approach</li>
                    </ul>
                </div>
                <div class="col-md-4 col-12">
                    <?php
                if ( is_user_logged_in() ) {
                    ?>

                    <a href="tel:00330478875555"> +33 (0)4 78 87 55 55</a>
                    <p class="info">
                        <strong>Allo Sourcing</strong>
                        <br> 81 Ancienne Route National 7,
                        <br> 69570 Dardily, France
                    </p>
                 <?php
                }
                ?>
                    <ul class="social">
                        <li>
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCURzwYY5mJuM2RcH8GnpB0A" target="_blank">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>


<!-- Modal -->
<div class="modal fade" id="myModal0" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            <h2 class="modal-title">Make an appointment</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body">


                <?php echo do_shortcode( ' [contact-form-7 id="289" title="Form contact"]' ); ?>

            </div>

        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
           <div class="modal-header">
            <h2 class="modal-title">Ask for a call</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <div class="modal-body">


                <?php echo do_shortcode( ' [contact-form-7 id="289" title="Form contact"]' ); ?>

            </div>

        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Start the simulation</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <?php echo do_shortcode( ' [contact-form-7 id="290" title="Ask"]' ); ?>

            </div>

        </div>

    </div>
</div>



</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>
    <script src="http://dev.allo-sourcing.com/assets/js/FancyProductDesigner.js" type="text/javascript"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="http://dev.allo-sourcing.com/assets/js/main.js"></script>
<script type="text/javascript" src="http://dev.allo-sourcing.com/assets/slick/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js" type="text/javascript"></script>
<script>
        $(document).ready(function () {


            jQuery('#owl-carousel3').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:5
                    }
                }
            })

            $(document).on("click", ".fpd-grid-columns-2 .fpd-btn span", function() {

                $(".fpd-grid-columns-2  .fpd-icon-close").click();

            });

         jQuery(".owl-carousel").owlCarousel({
             nav:true,
             loop: true,
             autoHeight:true,
						 autoplay: true,
						 autoplayTimeout: 12000,
             navText: ["<img class='left-arr' src='<?php echo get_template_directory_uri(); ?>/img/right.png'>","<img class='right-arr' src='<?php echo get_template_directory_uri(); ?>/img/right.png'>"],
             responsive:{
                 0:{
                     items:1
                 }
             }
         });



           $(".fpd-close-dialog span ").click();


            jQuery('.product-list').slick({
			 			dots: false,
			 			arrows: true,
			 			infinite: true,
			 			speed: 300,
			 			slidesToShow: 4,
			 			slidesToScroll: 1,
						autoplay: false,
						autoplaySpeed: 3000,
			 			prevArrow:"<img class='a-left control-c prev slick-prev' src='<?php echo get_template_directory_uri(); ?>/img/right-black.png'> ",
			 			nextArrow:"<img class='a-right control-c next slick-next' src='<?php echo get_template_directory_uri(); ?>/img/right-black.png'>",
			 			responsive: [
			 					{
			 							breakpoint: 1024,
			 							settings: {
			 									slidesToShow: 3,
			 									slidesToScroll: 1,
			 									infinite: true,
												arrows: false,
			 							}
			 					},
			 					{
			 							breakpoint: 600,
			 							settings: {
			 									slidesToShow: 2,
			 									slidesToScroll: 1,
												arrows: false,
			 							}
			 					},
			 					{
			 							breakpoint: 480,
			 							settings: {
			 									slidesToShow: 1,
			 									slidesToScroll: 1,
												arrows: false,
			 							}
			 					}
			 					// You can unslick at a given breakpoint now by adding:
			 					// settings: "unslick"
			 					// instead of a settings object
			 			]
			 	});
			});


        $(window).on("load",function(e){



            $(".mobile-menu").click(function () {

                $(".mobile").toggle();


            });

            $(".mobile .menu li > span").click(function () {

                if ($(this).siblings(".submenu").css('display') == 'none') {
                    console.log("none");
                    $(".mobile .submenu").hide();
                    $(this).siblings(".submenu").show();
                } else {
                    console.log("block");
                    $(this).siblings(".submenu").hide();
                }


            })

            $(".mobile .menu-subtitle").click(function () {

                if ($(this).siblings(".submenu-menu").css('display') == 'none') {
                    console.log("none");
                    $(".mobile .submenu-menu").hide();
                    $(this).siblings(".submenu-menu").show();
                } else {
                    console.log("block");
                    $(this).siblings(".submenu-menu").hide();
                }
            })



        });

</script>
	<script>

	$(document).ready(function () {
    $('.tab').click(function () {
        $('.tab').removeClass('active')
        $(this).addClass('active')
        var index = $(this).index() + 1
        $('.tab-content > div').removeClass('current')
        $('.tab-content > div:nth-child( ' + index + ')').addClass('current')
        if (index == 3) {
            ReviewsOn()
        }
        else {
            ReviewsOff()
        }
    })

    $('.menu li > span').click(function (e) {
        e.stopPropagation()

        if ($(this).parent().hasClass('active')) {
            $(this).parent().removeClass('active')
            $('.submenu').removeClass('active');
            return false
        }
        $('.menu > li').removeClass('active')
        $(this).parent().addClass('active')
        $('.submenu').removeClass('active');
        $(this).parent().find('.submenu').addClass('active')

    })
    $('.submenu-tab').click(function (e) {
        e.stopPropagation()
        e.preventDefault();
        $('.submenu-tab').removeClass('active');
        $(this).addClass('active');
        var index = $(this).index() + 1
        $('.submenu-content').removeClass('current')
        $('.submenu-content:nth-child( ' + index + ')').addClass('current')
    })
    $('.submenu i ').click(function () {
        $('.submenu').removeClass('active');
        $('.menu > li').removeClass('active')
    })

    function ReviewsOn() {
        $('.reviews').slick({
            dots: false,
            arrows: true,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    }
    function ReviewsOff() {
        $('.reviews').slick('unslick');
    }


    // $("#video1").click(function () {
    //     $("#popoup").addClass('open');
    //     $("#popoup .video-box >div").removeClass('active');

    //     $("#popoup .video-box #video1").addClass('active');
    // })
    // $("#video2").click(function () {
    //     $("#popoup").addClass('open');
    //     $("#popoup .video-box >div").removeClass('active');

    //     $("#popoup .video-box #video2").addClass('active');
    // })
    // $("#video3").click(function () {
    //     $("#popoup").addClass('open');
    //     $("#popoup .video-box >div").removeClass('active');

    //     $("#popoup .video-box #video3").addClass('active');
    // })
    $("#go-to-customizer").click(function () {
        $("#popoup").addClass('open');
    })
    $("#contact-button").click(function () {
        $("#popoup").addClass('open');
    })
    $(".close-popup").click(function () {
        $('#popoup').removeClass('open')
    })
    $(".popup-overlay").click(function () {
        $('#popoup').removeClass('open')
    })


});
	</script>

<script>
$(document).ready(function(){
	//$('#fpd-start-customizing-button').html('');

	var container = $('.customize-container');

	$('.fpd-modal-product-designer').on('click', '.fpd-done', function(e) {
    e.preventDefault();
		container.html('');
      if($('.fpd-modal-product-designer').length > 0) {
				for (var i=0; fancyProductDesigner.viewInstances.length > i; i++) {
					fancyProductDesigner.viewInstances[i].toDataURL(function(dataURL) {
						container.append('<div class="customize-image"><img src="' + dataURL + '" alt="' + fancyProductDesigner.currentViews[i].title + '"/></div>');
					}, 'transparent', {format: 'png'});
				}
      }
  });
});
</script>
<script>
$(document).ready(function(){
	$('.fpd-modal-product-designer').on('click', function(e) {
		console.log('test');
		let container = $('.fpd-modal-wrapper')
		if (!container.is(e.target) && container.has(e.target).length === 0) {
			$('.fpd-modal-product-designer').css('display','none');
			$('body').removeClass('fpd-overflow-hidden');
		}
	});
});
</script>
<script>
	$(document).ready(function(){
		jQuery('.product-thumb-slider').slick({
				dots: false,
				arrows: true,
				infinite: true,
				speed: 300,
				draggable: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: false,
				autoplaySpeed: 3000,
				prevArrow: '<i class="fas fa-chevron-left small-prev"></i>',
				nextArrow: '<i class="fas fa-chevron-right small-next "></i>',

		});
		var currencySwitcher = $('#currency-switcher'),
				currency = localStorage.getItem('currency'),
				priceRangeEur = $('.price-range.eur'),
			  priceRangeUsd = $('.price-range.usd');
		currencySwitcher.change(function(){
			localStorage.setItem('currency', $(this).val());
			currency = localStorage.getItem('currency');
			if(currency === 'EUR') {
				currencySwitcher.val('EUR');
				$('body').removeClass('currency-usd').addClass('currency-eur');
				priceRangeUsd.hide();
	      priceRangeEur.show();
			} else {
				currencySwitcher.val('USD');
				$('body').removeClass('currency-eur').addClass('currency-usd');
				priceRangeEur.hide();
	      priceRangeUsd.show();
			}
		});
		if(currency === 'EUR') {
			currencySwitcher.val('EUR');
			$('body').removeClass('currency-usd').addClass('currency-eur');
			priceRangeUsd.hide();
			priceRangeEur.show();
		} else {
			currencySwitcher.val('USD');
			$('body').removeClass('currency-eur').addClass('currency-usd');
			priceRangeEur.hide();
			priceRangeUsd.show();
		}
		$('#gtranslate_selector option:nth(0)').remove();
		var lang = localStorage.getItem('lang');
		$('#gtranslate_selector').change(function(){
			localStorage.setItem('lang', $(this).val());
		});
		if( lang === 'en|en' || lang === null ) {
			$('#gtranslate_selector').val('en|en');
		} else {
			$('#gtranslate_selector').val('en|fr');
		}
	});
</script>
</body>

</html>
