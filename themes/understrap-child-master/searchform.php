<form class="search-form" role="search" method="get" id="searchform" action="<?php echo site_url('/shop/'); ?>">
  <div class="search-icon" id="searchicon">
      <img src="http://dev.allo-sourcing.com/assets/img/icones/magnifying-glass (1).png" alt="">
  </div>
  <input type="text" class="search-input" placeholder="<?php echo __('Search', 'allo'); ?>" value="" name="s" id="s">
</form>

<script text="javascript">
  $("#searchicon").click(function(){
    $("#searchform").submit();
  });
</script>
