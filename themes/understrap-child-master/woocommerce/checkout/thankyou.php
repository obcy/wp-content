<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>

<div class="woocommerce-order">

    <?php if ( $order ) : ?>

        <?php if ( $order->has_status( 'failed' ) ) : ?>

            <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

            <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
                <a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
                <?php if ( is_user_logged_in() ) : ?>
                    <a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My account', 'woocommerce' ); ?></a>
                <?php endif; ?>
            </p>

        <?php else : ?>

            <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your quote has been received.', 'woocommerce' ), $order ); ?></p>

            <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

                <li class="woocommerce-order-overview__order order">
                    <span><?php _e( 'Quote number:', 'woocommerce' ); ?></span>
                    <strong><?php echo $order->get_order_number(); ?></strong>
                </li>

                <li class="woocommerce-order-overview__date date">
                    <span><?php _e( 'Date:', 'woocommerce' ); ?></span>
                    <strong><?php echo wc_format_datetime( $order->get_date_created() ); ?></strong>
                </li>

                <?php if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
                    <li class="woocommerce-order-overview__email email">
                        <span><?php _e( 'Email:', 'woocommerce' ); ?></span>
                        <strong><?php echo $order->get_billing_email(); ?></strong>
                    </li>
                <?php endif; ?>

                <?php

                $test = "<p>Conditions générales de vente :<br />
Nous vous rappelons que notre prix comprend livraison 1 point sur palette perdue de dimension standard (80x120x180 - 100x120x180)</p>
<p>Condition de règlement pour les commandes spéciales:<br />
- 30 % d'acompte à la commande.<br />
- Le solde aux conditions habituelles à définir.<br />
La loi de modernisation de l’économie n°2008-776 du 4 Aout 2008 impose une réduction des délais de paiement, qui ne pourront plus dépasser 45 jours fin de mois ou 60 jours net pour toutes les factures émises à compter du 1er Janvier 2009.Nous sommes dans l’obligation de modifier nos conditions générales de vente à partir du 1er Janvier 2009. Par ailleurs, conformément à cette loi, le taux minimal des pénalités dues en cas de retard sera porté à 3 fois le taux légal.<br />
Cette action étant la stricte application de la loi, elle ne modifie en rien le reste de nos accords commerciaux.</p>
<p>Ces prix ne comprennent pas :<br />
- La livraison avec camion Hayon<br />
- Les Coûts sont donnés selon nos colisages</p>
<p>Ce nouveau tarif nous est imposé par nos fournisseurs et fabricants. Depuis plusieurs mois nous subissons des hausses importantes et continuelles sur nos approvisionnements. Ces hausses sont dues essentiellement à la plusieurs facteurs dont :<br />
- la hausse des matières premières (coton, polyester),<br />
- la hausse des coûts de main d’œuvre,<br />
- la hausse des frais d’approche,<br />
- et aussi à un taux de change (€/US$) particulièrement défavorable depuis plusieurs mois.</p>
<p>De plus, la situation dans les pays producteurs est particulièrement difficile avec des problèmes de productivité importante et une forte augmentation de la demande. Il y a par actuellement des ruptures importantes sur beaucoup de produits et des approvisionnements qui ne permettent pas de satisfaire l’ensemble de vos demandes.</p>
<p>BAT:<br />
Tolérances qualitatives et quantitatives :<br />
Malgré le soin apporté par notre Société à la sélection de ses fabricants et prestataires, il peut exister de légères différences entre le BAT et la production finale.<br />
Ceci concerne notamment :<br />
- les couleurs de la matière : il peut exister des nuances entre le BAT et la production, ainsi que dans la production elle-même. Ceci est inévitable en raison des différents bains utilisés pour teindre la matière. Selon la quantité commandée, il est parfois impossible de tout teindre en un seul bain (chaque bain pouvant avoir une nuance différente).<br />
- les couleurs du marquage : le procédé d’impression et les encres utilisées étant différentes pour le BAT et pour la production elle-même, le rendu peut être légèrement différent (surtout si le fond est de couleur sombre et l’encre decouleur claire).<br />
- les tailles : selon les normes de l’industrie textile, une tolérance de +/-5 % est tolérée.<br />
- le grammage ou l’épaisseur de la matière : selon les normes de l’industrie textile, une tolérance de +/- 5 à 8 % est tolérée.</p>
<p>Délais :<br />
-Les délais de BAT ainsi que les temps de productions inscrits dans votre devis restent valables sous réserve de la bonne réception de tous les éléments de marquage dans le même temps que la réception de votre confirmation de commande.<br />
-Les éléments de marquage doivent être exploitables et sous réserves de la faisabilité de ceux-ci sur le produits concerné. Pour information, les délais de productions seront retardes suite à toute modification du BAT<br />
-Possibilité de réduire les délais en faisant de l’aérien ceci moyennant un surcoût (nous consulter)-Respect Délai : Sous réserve de recevoir votre accord de BAT sous 48 heures-Retard Indépendant de notre volonté : Transbordement, grève des dockers, visite des douanes</p>
<p>Livraison<br />
Pour toutes instructions particulières, quand à la livraison et palettisation (camion hayon, transpalette, dimension spéciale de palette, palette Europe…), nous vous prions de bien vouloir nous prévenir au minimum 15 jours avant la livraison afin de vous établir un devis car toutes ces prestations sont à la charge de votre client.</p>";

                update_field('terms_and_conditions', $test, $order->get_id() ); ?>

                <!-- <li class="woocommerce-order-overview__total total">
					<?php _e( 'Total:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_formatted_order_total(); ?></strong>
				</li> -->

                <?php if ( $order->get_payment_method_title() ) : ?>
                    <li class="woocommerce-order-overview__payment-method method">
                        <?php _e( 'Payment method:', 'woocommerce' ); ?>
                        <strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
                    </li>
                <?php endif; ?>

            </ul>

        <?php endif; ?>

        <?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
        <?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

    <?php else : ?>

        <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your quote has been received.', 'woocommerce' ), null ); ?></p>

    <?php endif; ?>

</div>
