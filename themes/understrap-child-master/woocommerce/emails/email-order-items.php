<?php
/**
 * Email Order Items
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-items.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.0
 */
global $WOOCS;

defined( 'ABSPATH' ) || exit;

$text_align  = is_rtl() ? 'right' : 'left';
$margin_side = is_rtl() ? 'left' : 'right';

foreach ( $items as $item_id => $item ) :
	$product       = $item->get_product();
	$sku           = '';
	$purchase_note = '';
	$image         = '';

	if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
		continue;
	}

	if ( is_object( $product ) ) {
		$sku           = $product->get_sku();
		$purchase_note = $product->get_purchase_note();
		$image         = $product->get_image( $image_size );
	}

	?>
	<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
		<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap:break-word;">
		<?php

		// Show title/image etc.
		if ( $show_image ) {
			echo wp_kses_post( apply_filters( 'woocommerce_order_item_thumbnail', $image, $item ) );
		}

		// Product name.
    $nameArr = explode('-',$item->get_name(), 2);
    $name = $nameArr[0];
		echo wp_kses_post( apply_filters( 'woocommerce_order_item_name', $name, $item, false ) );

		// SKU.
		if ( $show_sku && $sku ) {
			echo wp_kses_post( ' (#' . $sku . ')' );
		}

		// allow other plugins to add additional product information here.
		do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order, $plain_text );

	  $color  =  wc_get_order_item_meta( $item_id, 'Color', true );

      $sizeS  =  wc_get_order_item_meta( $item_id, 'Size-S', true );

      $sizeM  =  wc_get_order_item_meta( $item_id, 'Size-M', true );

      $sizeL  =  wc_get_order_item_meta( $item_id, 'Size-L', true );

      $sizeXL =  wc_get_order_item_meta( $item_id, 'Size-XL', true );

      $sizeOrg = '';

      $options =  wc_get_order_item_meta( $item_id, 'Options', true );

      $remarks =  wc_get_order_item_meta( $item_id, 'Marks', true );

        ?>
    <ul style="list-style: none">

     <?php if ($color != ''){ ?>
      <li><?php echo __('Color:', 'sps') . ' ' . $color; ?></li>
     <?php } ?>

         <?php if ($sizeS != '' && $sizeS != 0){ ?>
      <li><?php echo __('Size S:', 'sps') . ' ' . $sizeS; ?></li>
      <?php } ?>
        <?php if ($sizeM != ''  && $sizeM != 0){ ?>
      <li><?php echo __('Size M:', 'sps') . ' ' . $sizeM; ?></li>
      <?php } ?>
         <?php if ($sizeL != ''  && $sizeL != 0){ ?>
      <li><?php echo __('Size L:', 'sps') . ' ' . $sizeL; ?></li>
      <?php } ?>
         <?php if ($sizeXL != ''  && $sizeXL != 0){ ?>
      <li><?php echo __('Size XL:', 'sps') . ' ' . $sizeXL; ?></li>
         <?php } ?>
      <?php if (   $sizeOrg != ''  && $sizeOrg != 0)  { ?>
      <li><?php echo __('Size Original:', 'sps') . ' ' . $sizeOrg; ?></li>
      <?php } ?>
   <?php if (   $options != ''  && $options != 0)  { ?>
      <li><?php echo __('Options:', 'sps') . ' ' . $options; ?></li>
   <?php } ?>
      <li><?php echo __('Marks:', 'sps') . ' ' . $remarks; ?></li>
    </ul>
    <?php

		do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order, $plain_text );

		?>
		</td>
		<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
			<?php echo wp_kses_post( apply_filters( 'woocommerce_email_order_item_quantity', $item->get_quantity(), $item ) ); ?>
		</td>
		<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
			<?php $currency = wc_get_order_item_meta( $item_id, 'Current-currency', true ); ?>
				<?php if ($currency == 'EUR') {
					echo '€';
				} else {
					echo '$';
				}
				?>
			<?php echo  wc_get_order_item_meta( $item_id, 'Price-range', true ); //echo wp_kses_post( $order->get_formatted_line_subtotal( $item ) ); ?>
		</td>
	</tr>


<?php endforeach; ?>
