<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<section id="feature-products">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="breadcrumbs">
					<?php woocommerce_breadcrumb(); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<h2 class="title">Our advices</h2>
			</div>
		</div>
		<div class="row our-adivces">
		<?php
			$cate = get_queried_object();
			$cateID = $cate->term_id;
			$args = array(
				'posts_per_page' => 4,
				'tax_query' => array(
					'relation' => 'OR',
					array(
						'taxonomy' => 'product_cat',
						'field' => 'id',
						'terms' => $cateID
					),
					array(
						'taxonomy' => 'product_cat',
						'field' => 'id',
						'terms' => '28'
					)
				),
				'post_type' => 'product',
				'orderby' => 'rand',
			);
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
				if ( $loop->have_posts() ) {
					while ( $loop->have_posts() ) : $loop->the_post();
						wc_get_template_part( 'content', 'product' );
					endwhile;
				} else {
					echo __( 'No products found' );
				}
			}
				wp_reset_postdata();
		?>
		</div>
	</div>
</section>
<section id="products" class="product-catalog">
	<div class="container fullwidth">
		<div class="row">
			<div class="col-md-3 col-12">
				<div class="filter-box">
					<p>
						<strong><?php echo __('Product characteristics', 'allo'); ?></strong>
					</p>
					<?php dynamic_sidebar( 'left-sidebar-1' ); ?>
					<p>
						<strong><?php echo __('Supplier characteristics', 'allo'); ?></strong>
					</p>
					<?php dynamic_sidebar( 'left-sidebar-2' ); ?>
					<p>
						<strong><?php echo __('Marking techniques', 'allo'); ?></strong>
					</p>
					<?php dynamic_sidebar( 'left-sidebar-3' ); ?>
				</div>
			</div>
			<div class="col-md-9 col-12">
				<div class="sort-box">
					<div class="quantity">
							<span>Quantity: </span>
							<input type="text">
					</div>
					<div class="price">
						<?php dynamic_sidebar( 'right-sidebar' ); ?>
					</div>
				</div>
				<div class="product-list2">

				 <?php
					if ( woocommerce_product_loop() ) {

						/**
						 * Hook: woocommerce_before_shop_loop.
						 *
						 * @hooked woocommerce_output_all_notices - 10
						 * @hooked woocommerce_result_count - 20
						 * @hooked woocommerce_catalog_ordering - 30
						 */
						do_action( 'woocommerce_before_shop_loop' );

						woocommerce_product_loop_start();

						if ( wc_get_loop_prop( 'total' ) ) {
							while ( have_posts() ) {
								the_post();

								/**
								 * Hook: woocommerce_shop_loop.
								 *
								 * @hooked WC_Structured_Data::generate_product_data() - 10
								 */
								do_action( 'woocommerce_shop_loop' );

								wc_get_template_part( 'content', 'product' );
							}
						}

						woocommerce_product_loop_end();

						/**
						 * Hook: woocommerce_after_shop_loop.
						 *
						 * @hooked woocommerce_pagination - 10
						 */
						do_action( 'woocommerce_after_shop_loop' );
					} else {
						/**
						 * Hook: woocommerce_no_products_found.
						 *
						 * @hooked wc_no_products_found - 10
						 */
						do_action( 'woocommerce_no_products_found' );
					}


					?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
	/**
	* Hook: woocommerce_after_main_content.
	*
	* @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
	*/
	do_action( 'woocommerce_after_main_content' );
?>

<?php


get_footer( 'shop' );
