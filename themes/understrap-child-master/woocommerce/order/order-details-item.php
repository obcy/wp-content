<?php
/**
 * Order Item Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-item.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}
$items = $order->get_items();
?>
<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'woocommerce-table__line-item order_item', $item, $order ) ); ?>">

	<td class="woocommerce-table__product-name product-name">
		<?php
			$is_visible        = $product && $product->is_visible();
			$product_permalink = apply_filters( 'woocommerce_order_item_permalink', $is_visible ? $product->get_permalink( $item ) : '', $item, $order );
      $nameArr = explode('-', $item->get_name(), 2);
      $name = $nameArr[0];
      echo wp_kses_post( sprintf( '<span><a href="%s">%s</a>', esc_url( $product_permalink ), $name ));
			//echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $item->get_name() ) : $item->get_name(), $item, $is_visible );
			echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $item->get_quantity() ) . '</strong></span>', $item );

			do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order, false );

			//wc_display_item_meta( $item );
      //var_dump($item->get_meta_data('Color'));
      //echo wc_get_order_item_meta( $item_id, 'Color', true);
      foreach($items as $item) {
        $color = wc_get_order_item_meta( $item_id, 'sps_color', true );
        $sizeS = wc_get_order_item_meta( $item_id, 'sps_size_s', true );
        $sizeM = wc_get_order_item_meta( $item_id, 'sps_size_m', true );
        $sizeL = wc_get_order_item_meta( $item_id, 'sps_size_l', true );
        $sizeXL = wc_get_order_item_meta( $item_id, 'sps_size_xl', true );
				$mark = wc_get_order_item_meta( $item_id, 'sps_mark', true );
				$options = wc_get_order_item_meta( $item_id, 'sps_options', true );
      }?>
			<ul class="product-custom-details">
				<?php if ( $color ) : ?>
					<li><?php echo __('Color:', 'sps') . ' ' . $color; ?></li>
				<?php endif; ?>
				<?php if ( $sizeS ) : ?>
					<li><?php echo __('Size S:', 'sps') . ' ' . $sizeS; ?></li>
				<?php endif; ?>
				<?php if ( $sizeM ) : ?>
					<li><?php echo __('Size M:', 'sps') . ' ' . $sizeM; ?></li>
				<?php endif; ?>
				<?php if ( $sizeL ) : ?>
					<li><?php echo __('Size L:', 'sps') . ' ' . $sizeL; ?></li>
				<?php endif; ?>
				<?php if ( $sizeXL ) : ?>
					<li><?php echo __('Size XL:', 'sps') . ' ' . $sizeXL; ?></li>
				<?php endif; ?>
				<?php if ( $mark ) : ?>
					<li><?php echo __('Marking technique:', 'sps') . ' ' . $mark; ?></li>
				<?php endif; ?>
				<?php if ( $options ) : ?>
					<li><?php echo __('Options:', 'sps') . ' ' . $options; ?></li>
				<?php endif; ?>
	    </ul>
      <?php
			do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order, false );
		?>
	</td>
	<td class="woocommerce-table__product-total product-total"><div class="price-range">
    <?php
      $priceRange = wc_get_order_item_meta( $item_id, 'sps_price_range', true );
      echo $priceRange;
    ?>
  </div>
	</td>

</tr>

<?php if ( $show_purchase_note && $purchase_note ) : ?>

<tr class="woocommerce-table__product-purchase-note product-purchase-note">

	<td colspan="2"><?php echo wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) ); ?></td>

</tr>

<?php endif; ?>
