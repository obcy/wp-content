<section id="icons">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-12 text-center">
                <div class="map">
                    <img src="<?php echo get_site_url() . '/wp-content/uploads/2019/05/europe.png'; ?>" alt="">
                    <div class="map-text">
                        <p>More than
                            <span>10 000</span> dealers
                            <br/> in Europe</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="icon-box">
                    <img src="<?php echo get_site_url() . '/assets/img/icones/booking.png'; ?>" alt="">
                    <p id="contact-button" data-toggle="modal" data-target="#myModal0">Make an appointment</p>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="icon-box">
                    <img src="<?php echo get_site_url() . '/assets/img/icones/phone-call.png'; ?>" alt="">
                    <p  id="call-button" data-toggle="modal" data-target="#myModal">Ask for a call</p>
                </div>
            </div>
        </div>
    </div>
</section>
