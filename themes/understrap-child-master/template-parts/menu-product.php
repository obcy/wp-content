<?php
  $category_name = get_query_var('category_name');
  $query = new WC_Product_Query( array(
    'category' => $category_name,
    'limit' => 2,
    'orderby' => 'rand',
    'order' => 'rand',
    'hide_empty' => false,
  ) );

  $products = $query->get_products();

  foreach ( $products as $product ) {
    $price_range =  get_field('price_range', $product->id );
    $image_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'single-post-thumbnail' );
    $product_url = get_permalink( $product->id );
?>
    <a href="<?php echo $product_url; ?>">
      <div class="submenu-product">
          <div class="product-img">
              <img src="<?php echo $image_thumbnail[0]; ?>" alt="">
          </div>
          <p class="product-title"><?php echo $product->name; ?></p>
          <p class="price-range"><?php echo $price_range; ?></p>
      </div>
    </a>
<?php } ?>
