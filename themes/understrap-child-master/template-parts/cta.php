<section id="cta">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12">
                <h3><?php the_field('cta_title', 'option') ?></h3>
                <p><?php the_field('cta_text#1', 'option') ?></p>
            </div>
            <div class="col-md-6 col-12">
                <p><?php the_field('cta_text#2', 'option') ?></p>
                <button class="btn white-bg red-color">
                    <a href="<?php the_field('cta_button_url', 'option') ?>" data-toggle="modal" data-target="#myModal2" class=""><?php the_field('cta_button_text', 'option') ?></a>
                </button>
            </div>
        </div>
    </div>
</section>
