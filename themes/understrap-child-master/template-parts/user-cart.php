<a class="cart-box" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
  <?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

      $count = WC()->cart->cart_contents_count;
      ?><span class="cart-contents" >
          <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
        </span>

  <?php } ?>

</a>
