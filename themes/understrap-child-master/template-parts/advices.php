<?php
  $args = array(
    'post_type' => 'product',
    'product_cat' => 'advices'
  );

  $advices_products = get_posts($args);
?>

<section id="products">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="title">OUR ADVICES</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="product-list">
                  <?php
                      $args = array(
                        'post_type' => 'product',
                        'posts_per_page' => 6,
                        'orderby' => 'rand',
                        'tax_query' => array(
                              array(
                                  'taxonomy' => 'product_cat',
                                  'field' => 'slug',
                                  'terms' => 'advices'
                              ),
                          ),
                        );

                        $loop = new WP_Query( $args );

                        if ( $loop->have_posts() ) {
                          if ( $loop->have_posts() ) {
                            while ( $loop->have_posts() ) : $loop->the_post();
                              wc_get_template_part( 'content', 'product' );
                            endwhile;
                          } else {
                            echo __( 'No products found' );
                          }
                        }
                        wp_reset_postdata();
                  ?>
                </div>
            </div>
        </div>
    </div>
</section>
