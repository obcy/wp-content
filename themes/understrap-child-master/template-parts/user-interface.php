<?php
    //$current_user = wp_get_current_user();
    //$shop_url = $woocommerce->shop->get_shop_url();
    /**
     * @example Safe usage:
     * $current_user = wp_get_current_user();
     * if ( ! $current_user->exists() ) {
     *     return;
     * }
     */
    // echo 'Username: ' . $current_user->user_login . '<br />';
    // echo 'User email: ' . $current_user->user_email . '<br />';
    // echo 'User first name: ' . $current_user->user_firstname . '<br />';
    // echo 'User last name: ' . $current_user->user_lastname . '<br />';
    // echo 'User display name: ' . $current_user->display_name . '<br />';
    // echo 'User ID: ' . $current_user->ID . '<br />';
?>
<div class="dropdown">
  <div class="dropdown-button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <img src="<?php echo get_site_url() . '/assets/img/icones/profile.png'; ?>" alt="profile-icon"/>
  </div>
  <?php if (is_user_logged_in()) {?>
    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
      <a class="dropdown-user" href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>"><?php echo __('My account', 'allo-sourcing'); ?></a>
      <a class="dropdown-user" href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ).'quotes'; ?>"><?php echo __('Quotes checking', 'allo-sourcing'); ?></a>
      <a class="dropdown-user" href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ).'previous-quotes'; ?>"><?php echo __('Previous quotes', 'allo-sourcing'); ?></a>
      <a class="dropdown-user" href="<?php echo get_site_url().'/wishlist'; ?>"><?php echo __('My wishlist', 'allo-sourcing'); ?></a>
      <a class="dropdown-user" href="<?php echo wc_logout_url(); ?>"><?php echo __('Logout', 'allo-sourcing'); ?></a>
    </div>
<?php } else { ?>
  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
    <a class="dropdown-user" href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>"><?php echo __('Login', 'allo-sourcing'); ?></a>
    <a class="dropdown-user" href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>"><?php echo __('Register', 'allo-sourcing'); ?></a>
  </div>
<?php } ?>
</div>
