<?php
  $args = array(
    'taxonomy' => 'product_cat',
    'orderby' => 'include',
    //'order' => 'ASC',
    'include' => array(120, 44, 16, 121, 122, 123, 124, 125),
    // 'orderby_include' => true,
    //'exclude' => 'advices',
    'hide_empty' => false,
    'parent' => 0
  );

  $categories = get_terms($args);

  foreach ( $categories as $cat ) {
    set_query_var('category_name', $cat->name); ?>
    <li>
        <span><?php echo $cat->name; ?></span>
        <div class="submenu">
            <div class="submenu-content-box">
                <?php
                    $args = array(
                      'taxonomy' => 'product_cat',
                      'parent' => $cat->term_id,
                      'hide_empty' => false,
                    );

                    $categories_child = get_terms( $args );

                    foreach ( $categories_child as $cat_child ) { ?>
                    <div class="submenu-content">
                      <p class="menu-subtitle"><?php echo $cat_child->name; ?></p>
                      <ul class="submenu-menu">
                    <?php
                        $args = array(
                          'taxonomy' => 'product_cat',
                          'parent' => $cat_child->term_id,
                          'hide_empty' => false,
                        );

                        $categories_grandson = get_terms( $args );

                        foreach ( $categories_grandson as $cat_grandson ) { ?>
                          <li>
                              <a href="<?php echo get_term_link( $cat_grandson->term_id ,'product_cat'); ?>"><?php echo $cat_grandson->name; ?></a>
                          </li>
                  <?php } ?>
              </ul>
            </div>
        <?php } ?>
      </div>
      <div class="menu-product-box">
          <?php get_template_part( 'template-parts/menu', 'product' ); ?>
      </div>
    </div>
  </li>
<?php } ?>
