<div class="main-menu desktop">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="menu">
                    <li class="allo">
                        <span>Allo-sourcing</span>
                        <div class="submenu">
                            <div class="submenu-content-box">
                                <div class="submenu-content">
                                    <ul class="submenu-menu">
                                        <li>
                                            <a href="javascript:void(0)">Why work with us ?</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo get_site_url() . '/our-main-figures/'; ?>">Our main figures</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Our offices in the world</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo get_site_url() . '/our-skills/'; ?>">Our skills</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Our values</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php get_template_part( 'template-parts/menu', 'categories' ); ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="main-menu mobile">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="menu">
                    <li class="allo">
                        <span>Allo-sourcing</span>
                        <div class="submenu">
                            <div class="submenu-content-box">
                                <div class="submenu-content">
                                    <ul class="submenu-menu-allo">
                                        <li>
                                            <a href="javascript:void(0)">Why work with us ?</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo get_site_url() . '/our-main-figures/'; ?>">Our main figures</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Our offices in the world</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo get_site_url() . '/our-skills/'; ?>">Our skills</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Our values</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php get_template_part( 'template-parts/menu', 'categories' ); ?>
                </ul>
            </div>
        </div>
    </div>
</div>
