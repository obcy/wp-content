<div class="row berocket_aapf_features">
<div class="col-md-6 col-xs-12">
<h4>Choose best layout for your filter</h4>

<h5>Our WooCommerce AJAX Products Filter offers several layouts including slider, image and color</h5>

<div class="demo-site"><a href="http://woocommerce-products-filter.berocket.com/shop/" target="_blank">[ demo site ]</a></div>
<img alt="Choose best layout for your filter" src="https://berocket.com/images/features/filters/filters-types.jpg" /></div>

<div class="col-md-6 col-xs-12">
<h4>Pagination and Order without page reloading</h4>

<h5>Users will get next page loaded ordered by their preferences and without page reloading</h5>

<div class="demo-site"><a href="http://woocommerce-products-filter.berocket.com/shop/" target="_blank">[ demo site ]</a></div>
<img alt="Pagination and Order without page reloading" src="https://berocket.com/images/features/filters/ajax_order_nav.jpg" /></div>
</div>

<div>&nbsp;</div>

<div class="row berocket_aapf_features">
    <div class="col-md-6 col-xs-12">
        <h4>Show filters above products</h4>

        <h5>Need more space for the products? Say no to sidebars</h5>

        <div class="demo-site">
            <a href="http://woocommerce-products-filter.berocket.com/product-category/tshirts/" target="_blank">[ demo site ]</a>
        </div>
        <img alt="Show filters above products" src="https://berocket.com/images/features/filters/above_products.jpg" />
    </div>

    <div>
        <h4>SEO friendly urls</h4>

        <h5>Use Nice URLs with canonicalization and slugs instead of IDs</h5>

        <div class="demo-site">
            <a href="" target="_blank">[ demo site ]</a>
        </div>

        <div>&nbsp;</div>
        <img alt="SEO friendly urls" src="https://berocket.com/images/features/filters/urls.jpg" />
    </div>
    
    <div>
        <h4>Show results before filtering</h4>

        <h5>Show users what they will get before filters applied. Fast and efficient</h5>

        <div class="demo-site">
            <a href="http://woocommerce-products-filter.berocket.com/woocommerce-products-filter-demo-4/" target="_blank">[ demo site ]</a>
        </div>

        <div>&nbsp;</div>

        <img alt="Show results before filtering" src="https://berocket.com/images/features/filters/before_filter.jpg" />
    </div>

    <div>
        <h4>Include/Exclude values</h4>

        <h5>Show only needed values from the attribute or hide few</h5>

        <div class="demo-site">
            <a href="http://woocommerce-products-filter.berocket.com/woocommerce-products-filter-demo-4/" target="_blank">[ demo site ]</a>
        </div>
        <img alt="Include/Exclude values" src="https://berocket.com/images/features/filters/include_exclude.jpg" />
    </div>

    <div>
        <h4>Search Box on any page</h4>

        <h5>Users can pre-fill filters before visiting shop page</h5>

        <div class="demo-site">
            <a href="http://woocommerce-products-filter.berocket.com/" target="_blank">[ demo site ]</a>
        </div>
        <img alt="Search Box on any page" src="https://berocket.com/images/features/filters/searchbox.jpg" />
    </div>

    <div>
        <h4>Update filters with user needs</h4>

        <h5>Hate &quot;No Products!&quot; message? Hide values without products on the go</h5>

        <div class="demo-site">
            <a href="http://woocommerce-products-filter.berocket.com/shop/" target="_blank">[ demo site ]</a>
        </div>

        <div>&nbsp;</div>
        <img alt="Update filters with user needs" src="https://berocket.com/images/features/filters/reload.jpg" />
    </div>

    <div>
        <h4>Brands<br />
        <small>(require our Brands plugin)</small></h4>

        <h5>Users love brands. Help them find favorite brands faster</h5>

        <div class="demo-site">
            <a href="http://woocommerce-products-filter.berocket.com/woocommerce-products-filter-demo-5/" target="_blank">[ demo site ]</a>
        </div>
        <img alt="Brands" src="https://berocket.com/images/features/filters/brands.jpg" />
    </div>

    <div>
        <h4>Price Ranges and Sliders for Attributes</h4>

        <h5>Love sliders? Use them now for the attributes too!</h5>

        <div class="demo-site">
            <a href="http://woocommerce-products-filter.berocket.com/woocommerce-products-filter-demo-2/" target="_blank">[ demo site ]</a>
        </div>
        <img alt="Price Ranges and Sliders for Attributes" src="https://berocket.com/images/features/filters/range_slider.jpg" />
    </div>


    <div>
        <h4>WPML and Polylang compatibility</h4>

        <h5>EASILY TRANSLATE THE PLUGIN TO OTHER LANGUAGES WITH THE POWERFUL WPML AND POLYLANG TOOLS</h5>
        <img alt="WPML and Polylang compatibility" src="https://berocket.com/img/docs/wpml_polylang.png" />
    </div>
</div>
<style>
.berocket_aapf_features img {
    max-width: 100%;
    margin:auto;
    display: block;
}
.berocket_aapf_features h4,
.berocket_aapf_features h5,
.berocket_aapf_features .demo-site {
    text-align: center;
}
.berocket_aapf_features h5 {
    font-size: 17px;
    font-weight: 500;
}
.berocket_aapf_features h4 {
    font-size: 26px;
    font-weight: 700;
    text-transform: uppercase;
}
</style>
