<?php
/**
 * Plugin Name: Advanced AJAX Product Filters for WooCommerce
 * Plugin URI: https://wordpress.org/plugins/woocommerce-ajax-filters/
 * Description: Unlimited AJAX products filters to make your shop perfect
 * Version: 1.3.0.2
 * Author: BeRocket
 * Requires at least: 4.0
 * Author URI: http://berocket.com
 * License: Berocket License
 * License URI: http://berocket.com/license
 * Text Domain: BeRocket_AJAX_domain
 * Domain Path: /languages/
 * WC tested up to: 3.5.3
 */
define( "BeRocket_AJAX_filters_version", '1.3.0.2' );
define( "BeRocket_AJAX_filters_file", __FILE__ );
include_once('main.php');
