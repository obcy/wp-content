<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
?>

    <div class="woocommerce-tabs wc-tabs-wrapper">
        <ul class="tabs wc-tabs" role="tablist">

            <li class="<?php echo esc_attr( $key ); ?>_tab" id="tab-title-1" role="tab" aria-controls="tab-1">
                <a href="#tab-1">Details</a>
            </li>
            <li class="<?php echo esc_attr( $key ); ?>_tab" id="tab-title-2" role="tab" aria-controls="tab-2">
                <a href="#tab-2">Technical Information</a>
            </li>
            <li class="<?php echo esc_attr( $key ); ?>_tab" id="tab-title-3" role="tab" aria-controls="tab-3">
                <a href="#tab-3">Customer reviews</a>
            </li>
            <li class="<?php echo esc_attr( $key ); ?>_tab" id="tab-title-4" role="tab" aria-controls="tab-4">
                <a href="#tab-4">Delivery</a>
            </li>
            <li class="<?php echo esc_attr( $key ); ?>_tab" id="tab-title-5" role="tab" aria-controls="tab-5">
                <a href="#tab-5">Pricing</a>
            </li>
        </ul>

        <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--1 panel entry-content wc-tab" id="tab-1" role="tabpanel" aria-labelledby="tab-title-1">
           <div class="px-4"> <?php the_content(); ?></div>
        </div>

        <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--2 panel entry-content wc-tab" id="tab-2" role="tabpanel" aria-labelledby="tab-title-2">

            <?php

            // check if the repeater field has rows of data
            if( have_rows('technical_description') ):

                // loop through the rows of data
                while ( have_rows('technical_description') ) : the_row();
             ?>

             <div class="row py-2 px-4">

               <div class="col-sm-4"><?php the_sub_field('name'); ?></div>
               <div class="col-sm-8"><?php the_sub_field('value'); ?></div>

             </div>

                <?php   endwhile;

            else :

                // no rows found

            endif;

            ?>



        </div>

        <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--3 panel entry-content wc-tab" id="tab-3" role="tabpanel" aria-labelledby="tab-title-3">

        <div class="px-4"><?php comments_template(); ?></div>

        </div>
        <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--4 panel entry-content wc-tab" id="tab-4" role="tabpanel" aria-labelledby="tab-title-4">

        <div class="px-4">  <?php the_field('delivery'); ?></div>


        </div>
        <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--5 panel entry-content wc-tab" id="tab-5" role="tabpanel" aria-labelledby="tab-title-5">


            <?php

            // check if the repeater field has rows of data
            if( have_rows('price_tab') ):

                // loop through the rows of data
                while ( have_rows('price_tab') ) : the_row();
                    ?>

                    <div class="row py-2 px-4">

                        <div class="col-sm-2"><?php the_sub_field('name'); ?></div>
                        <div class="col-sm-2"><?php the_sub_field('value1'); ?></div>
                        <div class="col-sm-2"><?php the_sub_field('value2'); ?></div>
                        <div class="col-sm-2"><?php the_sub_field('value3'); ?></div>
                        <div class="col-sm-2"><?php the_sub_field('value4'); ?></div>

                    </div>

                <?php   endwhile;

            else :

            endif;

            ?>

        </div>

    </div>

<?php  ?>