<?php
/**
 * Shows an order item
 *
 * @var object $item The item being displayed
 * @var int $item_id The id of the item being displayed
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once('../config.php');

$product      = $item->get_product();
$product_link = $product ? admin_url( 'post.php?post=' . $item->get_product_id() . '&action=edit' ) : '';
$thumbnail    = $product ? apply_filters( 'woocommerce_admin_order_item_thumbnail', $product->get_image( 'thumbnail', array( 'title' => '' ), false ), $item_id, $item ) : '';
$row_class    = apply_filters( 'woocommerce_admin_html_order_item_class', ! empty( $class ) ? $class : '', $item, $order );

$id = esc_attr( $item_id );
$sql_customize_image = "SELECT * FROM wp_allowoocommerce_order_itemmeta WHERE order_item_id = '$id' AND meta_key = 'Image'";
$result_customize_image = $conn->query($sql_customize_image);

$sql_image_quantity = "SELECT * FROM wp_allowoocommerce_order_itemmeta WHERE order_item_id = '$id' AND meta_key = 'images_quantity'";
$result_image_quantity = $conn->query($sql_image_quantity);

$result_get_images = $wpdb->get_results("SELECT * FROM wp_allowoocommerce_order_itemmeta WHERE order_item_id = '$id' AND meta_key LIKE '%image_$id%'");

if($result_image_quantity->num_rows > 0) {
	$images_quantity = $result_image_quantity->fetch_assoc();
} else {
	$images_quantity['meta_value'] = 0;
}

?>
<tr class="item <?php echo esc_attr( $row_class ); ?>" data-order_item_id="<?php echo esc_attr( $item_id ); ?>">
	<td class="thumb">
		<?php if ($result_customize_image->num_rows > 0) {
			$row = $result_customize_image->fetch_assoc();
			?>
			<div class="wc-order-item-thumbnail">
					<img width="300" height="300" src="<?php echo $row['meta_value']; ?>" class="attachment-thumbnail size-thumbnail" alt="" title="">
			</div>
		<?php
		} else {
			echo '<div class="wc-order-item-thumbnail">' . wp_kses_post( $thumbnail ) . '</div>';
		} ?>
		<?php  ?>
		<div class="wc-order-item-images" data-images-quantity="<?php echo $images_quantity['meta_value']; ?>">
			<?php if($result_get_images) {
							foreach ($result_get_images as $image) { ?>
								<div class="wc-order-item-image" data-image-id="<?php echo $image->meta_key; ?>">
									<img src="<?php echo $image->meta_value; ?>" alt="<?php echo $image->meta_key; ?>"/>
									<div class="wc-order-item-delete button">x</div>
								</div>
					<?php		}
			 			} ?>
			<div class="wc-order-item-image wc-order-item-add">
				<div class="add"></div>
			</div>
		</div>
	<td class="name" data-sort-value="<?php echo esc_attr( $item->get_name() ); ?>">
		<?php
		echo $product_link ? '<a href="' . esc_url( $product_link ) . '" class="wc-order-item-name">' . wp_kses_post( $item->get_name() ) . '</a>' : '<div class="wc-order-item-name">' . wp_kses_post( $item->get_name() ) . '</div>';
		$userMeta = get_user_meta($post->post_author);
		($userMeta['billing_address_1']) ? $address = '<div class="supplier-address"><strong>Address: </strong>' . implode(', ', $userMeta['billing_address_1']) . '</div>' : '';
		($userMeta['billing_address_1'] && $userMeta['billing_address_2'])? $address = '<div class="supplier-address"><strong>Address: </strong>' . implode(', ', $userMeta['billing_address_1']) . ' ' . implode(', ', $userMeta['billing_address_2']) . '</div>' : '';
		echo ($userMeta['billing_first_name']) ? '<div class="supplier-address"><strong>Name: </strong>' . implode(', ', $userMeta['billing_first_name']) . '</div>' : '';
		echo ($userMeta['billing_last_name']) ? '<div class="supplier-address"><strong>Surname: </strong>' . implode(', ', $userMeta['billing_last_name']) . '</div>' : '';
		echo ($userMeta['billing_company']) ? '<div class="supplier-address"><strong>Company: </strong>' . implode(', ', $userMeta['billing_company']) . '</div>' : '';
		echo $address;
		echo ($userMeta['billing_city']) ? '<div class="supplier-address"><strong>City: </strong>' . implode(', ', $userMeta['billing_city']) . '</div>' : '';
		echo ($userMeta['billing_postcode']) ? '<div class="supplier-address"><strong>Postcode: </strong>' . implode(', ', $userMeta['billing_postcode']) . '</div>' : '';
		echo ($userMeta['billing_country']) ? '<div class="supplier-address"><strong>Country: </strong>' . implode(', ', $userMeta['billing_country']) . '</div>' : '';
		echo ($userMeta['billing_state']) ? '<div class="supplier-address"><strong>State: </strong>' . implode(', ', $userMeta['billing_state']) . '</div>' : '';
		echo ($userMeta['billing_email']) ? '<div class="supplier-address"><strong>Email: </strong>' . implode(', ', $userMeta['billing_email']) . '</div>' : '';
		echo ($userMeta['billing_phone']) ? '<div class="supplier-address"><strong>Phone: </strong>' . implode(', ', $userMeta['billing_phone']) . '</div>' : '';
		if ( $product && $product->get_sku() ) {
			echo '<div class="wc-order-item-sku"><strong>' . esc_html__( 'Supplier reference:', 'woocommerce' ) . '</strong> ' . esc_html( $product->get_sku() ) . '</div>';
		}

		if ( $item->get_variation_id() ) {
			echo '<div class="wc-order-item-variation"><strong>' . esc_html__( 'Variation ID:', 'woocommerce' ) . '</strong> ';
			if ( 'product_variation' === get_post_type( $item->get_variation_id() ) ) {
				echo esc_html( $item->get_variation_id() );
			} else {
				/* translators: %s: variation id */
				printf( esc_html__( '%s (No longer exists)', 'woocommerce' ), $item->get_variation_id() );
			}
			echo '</div>';
		}
		?>
		<input type="hidden" class="order_item_id" name="order_item_id[]" value="<?php echo esc_attr( $item_id ); ?>" />
		<input type="hidden" name="order_item_tax_class[<?php echo absint( $item_id ); ?>]" value="<?php echo esc_attr( $item->get_tax_class() ); ?>" />

		<?php do_action( 'woocommerce_before_order_itemmeta', $item_id, $item, $product ); ?>
		<?php require 'html-order-item-meta.php'; ?>
		<?php do_action( 'woocommerce_after_order_itemmeta', $item_id, $item, $product ); ?>
	</td>

	<?php do_action( 'woocommerce_admin_order_item_values', $product, $item, absint( $item_id ) ); ?>

	<td class="item_cost" width="1%" data-sort-value="<?php echo esc_attr( $order->get_item_subtotal( $item, false, true ) ); ?>">
		<div class="view">
			<?php
				echo wc_price( $order->get_item_total( $item, false, true ), array( 'currency' => $order->get_currency() ) );

			if ( $item->get_subtotal() !== $item->get_total() ) {
				echo '<span class="wc-order-item-discount">-' . wc_price( wc_format_decimal( $order->get_item_subtotal( $item, false, false ) - $order->get_item_total( $item, false, false ), '' ), array( 'currency' => $order->get_currency() ) ) . '</span>';
			}
			?>
		</div>
	</td>
	<td class="quantity" width="1%">
		<div class="view">
			<?php
				echo '<small class="times">&times;</small> ' . esc_html( $item->get_quantity() );

			if ( $refunded_qty = $order->get_qty_refunded_for_item( $item_id ) ) {
				echo '<small class="refunded">-' . ( $refunded_qty * -1 ) . '</small>';
			}
			?>
		</div>
		<div class="edit" style="display: none;">
			<input type="number" step="<?php echo esc_attr( apply_filters( 'woocommerce_quantity_input_step', '1', $product ) ); ?>" min="0" autocomplete="off" name="order_item_qty[<?php echo absint( $item_id ); ?>]" placeholder="0" value="<?php echo esc_attr( $item->get_quantity() ); ?>" data-qty="<?php echo esc_attr( $item->get_quantity() ); ?>" size="4" class="quantity" />
		</div>
		<div class="refund" style="display: none;">
			<input type="number" step="<?php echo esc_attr( apply_filters( 'woocommerce_quantity_input_step', '1', $product ) ); ?>" min="0" max="<?php echo absint( $item->get_quantity() ); ?>" autocomplete="off" name="refund_order_item_qty[<?php echo absint( $item_id ); ?>]" placeholder="0" size="4" class="refund_order_item_qty" />
		</div>
	</td>
	<td class="line_cost" width="1%" data-sort-value="<?php echo esc_attr( $item->get_total() ); ?>">
		<div class="view">
			<?php
			echo wc_price( $item->get_total(), array( 'currency' => $order->get_currency() ) );

			if ( $item->get_subtotal() !== $item->get_total() ) {
				echo '<span class="wc-order-item-discount">-' . wc_price( wc_format_decimal( $item->get_subtotal() - $item->get_total(), '' ), array( 'currency' => $order->get_currency() ) ) . '</span>';
			}

			if ( $refunded = $order->get_total_refunded_for_item( $item_id ) ) {
				echo '<small class="refunded">-' . wc_price( $refunded, array( 'currency' => $order->get_currency() ) ) . '</small>';
			}
			?>
		</div>
		<div class="edit" style="display: none;">
			<div class="split-input">
				<div class="input">
					<label><?php esc_attr_e( 'Pre-discount:', 'woocommerce' ); ?></label>
					<input type="text" name="line_subtotal[<?php echo absint( $item_id ); ?>]" placeholder="<?php echo esc_attr( wc_format_localized_price( 0 ) ); ?>" value="<?php echo esc_attr( wc_format_localized_price( $item->get_subtotal() ) ); ?>" class="line_subtotal wc_input_price" data-subtotal="<?php echo esc_attr( wc_format_localized_price( $item->get_subtotal() ) ); ?>" />
				</div>
				<div class="input">
					<label><?php esc_attr_e( 'Total:', 'woocommerce' ); ?></label>
					<input type="text" name="line_total[<?php echo absint( $item_id ); ?>]" placeholder="<?php echo esc_attr( wc_format_localized_price( 0 ) ); ?>" value="<?php echo esc_attr( wc_format_localized_price( $item->get_total() ) ); ?>" class="line_total wc_input_price" data-tip="<?php esc_attr_e( 'After pre-tax discounts.', 'woocommerce' ); ?>" data-total="<?php echo esc_attr( wc_format_localized_price( $item->get_total() ) ); ?>" />
				</div>
			</div>
		</div>
		<div class="refund" style="display: none;">
			<input type="text" name="refund_line_total[<?php echo absint( $item_id ); ?>]" placeholder="<?php echo esc_attr( wc_format_localized_price( 0 ) ); ?>" class="refund_line_total wc_input_price" />
		</div>
	</td>

	<?php
	if ( ( $tax_data = $item->get_taxes() ) && wc_tax_enabled() ) {
		foreach ( $order_taxes as $tax_item ) {
			$tax_item_id       = $tax_item->get_rate_id();
			$tax_item_total    = isset( $tax_data['total'][ $tax_item_id ] ) ? $tax_data['total'][ $tax_item_id ] : '';
			$tax_item_subtotal = isset( $tax_data['subtotal'][ $tax_item_id ] ) ? $tax_data['subtotal'][ $tax_item_id ] : '';
			?>
			<td class="line_tax" width="1%">
				<div class="view">
					<?php
					if ( '' !== $tax_item_total ) {
						echo wc_price( wc_round_tax_total( $tax_item_total ), array( 'currency' => $order->get_currency() ) );
					} else {
						echo '&ndash;';
					}

					if ( $item->get_subtotal() !== $item->get_total() ) {
						if ( '' === $tax_item_total ) {
							echo '<span class="wc-order-item-discount">&ndash;</span>';
						} else {
							echo '<span class="wc-order-item-discount">-' . wc_price( wc_round_tax_total( $tax_item_subtotal - $tax_item_total ), array( 'currency' => $order->get_currency() ) ) . '</span>';
						}
					}

					if ( $refunded = $order->get_tax_refunded_for_item( $item_id, $tax_item_id ) ) {
						echo '<small class="refunded">-' . wc_price( $refunded, array( 'currency' => $order->get_currency() ) ) . '</small>';
					}
					?>
				</div>
				<div class="edit" style="display: none;">
					<div class="split-input">
						<div class="input">
							<label><?php esc_attr_e( 'Pre-discount:', 'woocommerce' ); ?></label>
							<input type="text" name="line_subtotal_tax[<?php echo absint( $item_id ); ?>][<?php echo esc_attr( $tax_item_id ); ?>]" placeholder="<?php echo esc_attr( wc_format_localized_price( 0 ) ); ?>" value="<?php echo esc_attr( wc_format_localized_price( $tax_item_subtotal ) ); ?>" class="line_subtotal_tax wc_input_price" data-subtotal_tax="<?php echo esc_attr( wc_format_localized_price( $tax_item_subtotal ) ); ?>" data-tax_id="<?php echo esc_attr( $tax_item_id ); ?>" />
						</div>
						<div class="input">
							<label><?php esc_attr_e( 'Total:', 'woocommerce' ); ?></label>
							<input type="text" name="line_tax[<?php echo absint( $item_id ); ?>][<?php echo esc_attr( $tax_item_id ); ?>]" placeholder="<?php echo esc_attr( wc_format_localized_price( 0 ) ); ?>" value="<?php echo esc_attr( wc_format_localized_price( $tax_item_total ) ); ?>" class="line_tax wc_input_price" data-total_tax="<?php echo esc_attr( wc_format_localized_price( $tax_item_total ) ); ?>" data-tax_id="<?php echo esc_attr( $tax_item_id ); ?>" />
						</div>
					</div>
				</div>
				<div class="refund" style="display: none;">
					<input type="text" name="refund_line_tax[<?php echo absint( $item_id ); ?>][<?php echo esc_attr( $tax_item_id ); ?>]" placeholder="<?php echo esc_attr( wc_format_localized_price( 0 ) ); ?>" class="refund_line_tax wc_input_price" data-tax_id="<?php echo esc_attr( $tax_item_id ); ?>" />
				</div>
			</td>
			<?php
		}
	}
	?>
	<td class="wc-order-edit-line-item" width="1%">
		<div class="wc-order-edit-line-item-actions">
			<?php if ( $order->is_editable() ) : ?>
				<a class="edit-order-item tips" href="#" data-tip="<?php esc_attr_e( 'Edit item', 'woocommerce' ); ?>"></a><a class="delete-order-item tips" href="#" data-tip="<?php esc_attr_e( 'Delete item', 'woocommerce' ); ?>"></a>
			<?php endif; ?>
		</div>
	</td>
</tr>
