<?php
if ( !class_exists( 'SPS_After_Single_Product_Summary' ) ) {
	class SPS_After_Single_Product_Summary{
		function __construct() {
			add_action( 'woocommerce_before_add_to_cart_form', array( $this, 'sps_after_single_product_summary' ), 1, 0);
			add_action( 'woocommerce_before_add_to_cart_form', array( $this, 'sps_quantity_minimum'), 2, 0);
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'sps_custom_inputs'),20, 0);
			add_filter( 'woocommerce_add_cart_item_data',  array( $this, 'sps_add_text_to_cart_item'), 10, 3 );
			add_filter( 'woocommerce_add_cart_item_data',  array( $this, 'sps_display_text_in_cart'), 10, 2 );
			add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'sps_add_text_to_order' ), 10, 4);
			add_action( 'woocommerce_before_single_product_summary', array( $this, 'sps_add_options_marks' ), 10 );
			add_action( 'wp_footer', array( $this, 'sps_popup' ), 5 );
			add_action( 'wp_footer', array( $this, 'sps_add_comment_to_customization' ), 6);
            add_action( 'wp_footer', array( $this, 'sps_add_comment_to_customization2' ), 6);
		}

		/* Output engraving field.
		*/
		function sps_custom_inputs() {
			 global $product;
			 $dimension = get_field('dimension');
			 // if ( $product->get_id() !== 1741 ) {
				// 	 return;
			 // }

			 ?>
			 		<input type="hidden" value="" id="sps-color" name="sps-color">
					<?php if ( $product->get_attribute('pa_size')) : ?>
				 		<input type="hidden" value="0" id="sps-size-s" name="sps-size-s">
				 		<input type="hidden" value="0" id="sps-size-m" name="sps-size-m">
				 		<input type="hidden" value="0" id="sps-size-l" name="sps-size-l">
						<input type="hidden" value="0" id="sps-size-xl" name="sps-size-xl">
					<?php else : ?>
						<input type="hidden" value="0" id="sps-general-quantity" name="sps-general-quantity">
					<?php endif; ?>
					<input type="hidden" value="" id="sps-current-currency" name="sps-current-currency">
					<input type="hidden" value="" id="sps-price-range" name="sps-price-range">
					<input type="hidden" value="" id="sps-mark" name="sps-mark">
					<input type="hidden" value="" id="sps-options" name="sps-options">
					<input type="hidden" value="" id="sps-add-comment-to-customization" name="sps-add-comment-to-customization">
          <input type="hidden" value="" id="sps-add-comment-to-customization2" name="sps-add-comment-to-customization2">
					<input type="hidden" value="" id="sps-custom-description" name="sps-custom-description">
					<input type="hidden" value="45 days" id="sps-production" name="sps-production">
					<input type="hidden" value="50 days" id="sps-delivery" name="sps-delivery">
					<input type="hidden" value="48-72 H" id="sps-bat" name="sps-bat">
					<input type="hidden" value="10-15 days" id="sps-prototype" name="sps-prototype">
					<input type="hidden" value="<?php echo ($dimension['length']) ? $dimension['length'] : '--'; ?>" id="sps-dimension-length" name="sps-dimension-length">
					<input type="hidden" value="<?php echo ($dimension['width']) ? $dimension['width'] : '--'; ?>" id="sps-dimension-width" name="sps-dimension-width">
					<input type="hidden" value="<?php echo $dimension['height'] ? $dimension['height'] : '--'; ?>" id="sps-dimension-height" name="sps-dimension-height">
			 <?php
		}

		function sps_add_text_to_cart_item( $cart_item_data, $product_id, $variation_id ) {
				$color = filter_input( INPUT_POST, 'sps-color' );
				$sizeS = filter_input( INPUT_POST, 'sps-size-s' );
				$sizeM = filter_input( INPUT_POST, 'sps-size-m' );
				$sizeL = filter_input( INPUT_POST, 'sps-size-l' );
				$sizeXL = filter_input( INPUT_POST, 'sps-size-xl' );
				$currentCurrency = filter_input( INPUT_POST, 'sps-current-currency' );
				$priceRange = filter_input( INPUT_POST, 'sps-price-range' );
				$imageData = filter_input( INPUT_POST, 'fpd_product_thumbnail');
				$mark = filter_input( INPUT_POST, 'sps-mark' );
				$sizeUniversal = filter_input( INPUT_POST, 'sps-general-quantity' );
				$options = filter_input( INPUT_POST, 'sps-options' );
				$commentCustomization = filter_input( INPUT_POST, 'sps-add-comment-to-customization');
        $commentCustomization2 = filter_input( INPUT_POST, 'sps-add-comment-to-customization2');
				$customDescription = filter_input( INPUT_POST, 'sps-custom-description');
				$production = filter_input( INPUT_POST, 'sps-production');
				$delivery = filter_input( INPUT_POST, 'sps-delivery');
				$bat = filter_input( INPUT_POST, 'sps-bat');
				$prototype = filter_input( INPUT_POST, 'sps-prototype');
				$dimensionLength = filter_input( INPUT_POST, 'sps-dimension-length');
				$dimensionWidth = filter_input( INPUT_POST, 'sps-dimension-width');;
				$dimensionHeight = filter_input( INPUT_POST, 'sps-dimension-height');;
		    // if ( empty( $color ) ) {
		    //     return $cart_item_data;
		    // }
		    $cart_item_data['sps-color'] = $color;
				$cart_item_data['sps-size-s'] = $sizeS;
				$cart_item_data['sps-size-m'] = $sizeM;
				$cart_item_data['sps-size-l'] = $sizeL;
				$cart_item_data['sps-size-xl'] = $sizeXL;
				$cart_item_data['sps-current-currency'] = $currentCurrency;
				$cart_item_data['sps-price-range'] = $priceRange;
				$cart_item_data['sps-data-image'] = $imageData;
				$cart_item_data['sps-mark'] = $mark;
				$cart_item_data['sps-general-quantity'] = $sizeUniversal;
				$cart_item_data['sps-options'] = $options;
				$cart_item_data['sps-add-comment-to-customization'] = $commentCustomization;
				$cart_item_data['sps-add-comment-to-customization2'] = $commentCustomization2;
				$cart_item_data['sps-custom-description'] = $customDescription;
				$cart_item_data['sps-production'] = $production;
				$cart_item_data['sps-delivery'] = $delivery;
				$cart_item_data['sps-bat'] = $bat;
				$cart_item_data['sps-prototype'] = $prototype;
				$cart_item_data['sps-dimension-length'] = $dimensionLength;
				$cart_item_data['sps-dimension-width'] = $dimensionWidth;
				$cart_item_data['sps-dimension-height'] = $dimensionHeight;

		    return $cart_item_data;
		}

		function sps_display_text_in_cart( $item_data, $cart_item ) {
		    // if ( empty( $cart_item['iconic-engraving'] ) ) {
		    //     return $item_data;
		    // }
				//ssss
		    $item_data[] = array(
		        'key'     => __( 'Color', 'sps' ),
		        'value'   => wc_clean( $cart_item['sps-color'] ),
		        'display' => '',
		    );
				$item_data[] = array(
		        'key'     => __( 'Size S', 'sps' ),
		        'value'   => wc_clean( $cart_item['sps-size-s'] ),
		        'display' => '',
		    );
				$item_data[] = array(
		        'key'     => __( 'Size M', 'sps' ),
		        'value'   => wc_clean( $cart_item['sps-size-m'] ),
		        'display' => '',
		    );
				$item_data[] = array(
		        'key'     => __( 'Size L', 'sps' ),
		        'value'   => wc_clean( $cart_item['sps-size-l'] ),
		        'display' => '',
		    );
				$item_data[] = array(
		        'key'     => __( 'Size XL', 'sps' ),
		        'value'   => wc_clean( $cart_item['sps-size-xl'] ),
		        'display' => '',
		    );
				$item_data[] = array(
		        'key'     => __( 'Marking technique', 'sps' ),
		        'value'   => wc_clean( $cart_item['sps-mark'] ),
		        'display' => '',
		    );
				$item_data[] = array(
		        'key'     => __( 'Options', 'sps' ),
		        'value'   => wc_clean( $cart_item['sps-options'] ),
		        'display' => '',
		    );

            $item_data[] = array(
                'key'     => __( 'Comment', 'sps' ),
                'value'   => wc_clean( $cart_item['sps-add-comment-to-customization'] ),
                'display' => '',
            );

            $item_data[] = array(
                'key'     => __( 'Specify the delays', 'sps' ),
                'value'   => wc_clean( $cart_item['sps-add-comment-to-customization2'] ),
                'display' => '',
            );

		    return $item_data;
		}
		function sps_add_text_to_order( $item, $cart_item_key, $values, $order ) {
		    // if ( empty( $values['sps-color'] ) ) {
		    //     return;
		    // }
				// if ( empty( $values['sps-size-s'] ) ) {
		    //     return;
		    // }
				// if ( empty( $values['sps-size-m'] ) ) {
		    //     return;
		    // }
				// if ( empty( $values['sps-size-l'] ) ) {
		    //     return;
		    // }
				// if ( empty( $values['sps-size-xl'] ) ) {
		    //     return;
		    // }
				// if ( empty( $values['sps-price-range'] ) ) {
		    //     return;
		    // }
				// if ( empty( $values['sps-data-image'] ) ) {
		    //     return;
		    // }
				// if ( empty( $values['sps-mark'] ) ) {
		    //     return;
		    // }
				// if ( empty( $values['sps-general-quantity'] ) ) {
		    //     return;
		    // }

		    $item->add_meta_data( __( 'Color', 'sps' ), $values['sps-color'] );
				$item->add_meta_data( __( 'Size-S', 'sps' ), $values['sps-size-s'] );
				$item->add_meta_data( __( 'Size-M', 'sps' ), $values['sps-size-m'] );
				$item->add_meta_data( __( 'Size-L', 'sps' ), $values['sps-size-l'] );
				$item->add_meta_data( __( 'Size-XL', 'sps' ), $values['sps-size-xl'] );
				$item->add_meta_data( __( 'Current-currency', 'sps' ), $values['sps-current-currency'] );
				$item->add_meta_data( __( 'Price-range', 'sps' ), $values['sps-price-range'] );
				$item->add_meta_data( __( 'Image', 'sps' ), $values['sps-data-image'] );
				$item->add_meta_data( __( 'Marks', 'sps'), $values['sps-mark'] );
				$item->add_meta_data( __( 'Customer-quantity', 'sps'), $values['sps-general-quantity'] );
				$item->add_meta_data( __( 'Options', 'sps'), $values['sps-options'] );
				$item->add_meta_data( __( 'Custom-comment', 'sps'), $values['sps-add-comment-to-customization'] );
        $item->add_meta_data( __( 'Custom', 'sps'), $values['sps-add-comment-to-customization2'] );
				$item->add_meta_data( __( 'Custom-description', 'sps'), $values['sps-custom-description'] );
				$item->add_meta_data( __( 'Production', 'sps'), $values['sps-production'] );
				$item->add_meta_data( __( 'Delivery', 'sps'), $values['sps-delivery'] );
				$item->add_meta_data( __( 'BAT-Mail', 'sps'), $values['sps-bat'] );
				$item->add_meta_data( __( 'Prototype', 'sps'), $values['sps-prototype'] );

				$item->add_meta_data( __( 'Quantity-1', 'sps'), '----/---- €');
				$item->add_meta_data( __( 'Quantity-2', 'sps'), '----/---- €' );
				$item->add_meta_data( __( 'Quantity-3', 'sps'), '----/---- €' );
				$item->add_meta_data( __( 'Option-1', 'sps'), '---- €' );
				$item->add_meta_data( __( 'Option-2', 'sps'), '---- €' );
				$item->add_meta_data( __( 'Option-3', 'sps'), '---- €' );
				$item->add_meta_data( __( 'Length', 'sps'), $values['sps-dimension-length'] );
				$item->add_meta_data( __( 'Width', 'sps'), $values['sps-dimension-width'] );
				$item->add_meta_data( __( 'Height', 'sps'), $values['sps-dimension-height'] );
		}

		function sps_after_single_product_summary(){
			global $product;
			$output = '';
			$variation_product = '';
			$sale_price = 0;
			$regular_price = 0;

				?>
          <?php
            $sizes = $product->get_attribute( 'pa_size' );
            $size = explode(', ', $sizes);

            $colors = $product->get_attribute( 'pa_color' );
            $color = explode(', ', $colors);

            $product_id = $product->get_id();
            $cart_url = wc_get_cart_url();
          ?>
          <div class="sps">
						<?php if ( $colors ) : ?>
								<div class="sps-dropdown">
									<div class="sps-dropdown-button" data-color="<?php echo $color[0]; ?>">
										<div class="sps-dropdown-button-circle"></div>
									</div>
									<div class="sps-dropdown-colors">
								<?php
									$terms = get_the_terms( get_the_ID(), 'pa_color');
									foreach ($terms as $key => $term) {
										$color_picker = get_field('color_picker', $term->taxonomy . '_' . $term->term_id);?>
										<div class="sps-dropdown-circle<?php echo ($color[$key] == 'ZCustom') ? ' custom' : '' ;?>" style="background: <?php echo $color_picker; ?>;" data-color="<?php echo $color[$key]; ?>" data-color-hex="<?php echo $color_picker; ?>"></div>
									<?php } ?>
							</div>
						</div>
						<input id="sps-color-picker" style="display: none;" type="color" value=""/>
					<?php endif; ?>
					<?php if ( $sizes ) : ?>
						<?php for ( $i = 0; $i < sizeof($size); $i++ ) : ?>
            <div class="sps-group">
              <div><?php echo $size[$i]; ?></div>
              <div class="sps-quantity" data-size="<?php echo $size[$i]; ?>"><?php echo  woocommerce_quantity_input(); ?></div>
            </div>
					<?php endfor; ?>
            <!-- <div class="sps-group">
              <div><?php echo $size[1]; ?></div>
              <div class="sps-quantity" data-size="<?php echo $size[1]; ?>"><?php echo  woocommerce_quantity_input(); ?></div>
            </div>
            <div class="sps-group">
              <div><?php echo $size[0]; ?></div>
              <div class="sps-quantity" data-size="<?php echo $size[0]; ?>"><?php echo  woocommerce_quantity_input(); ?></div>
            </div>
            <div class="sps-group">
              <div><?php echo $size[3]; ?></div>
              <div class="sps-quantity" data-size="<?php echo $size[3]; ?>"><?php echo  woocommerce_quantity_input(); ?></div>
            </div> -->
				<?php else : ?>
					<div class="sps-group">
						<div class="sps-quantity" data-size="universal"><?php echo  woocommerce_quantity_input(); ?></div>
					</div>
				<?php endif; ?>
					</div>
					<?php
		}
		function sps_add_options_marks() {
			$options = get_field('options');
			$size = 5; ?>
			<div class="sps-content">
				<?php if ($options) {
				?>
					<div class="sps-options">
						<h2><?php echo __('Options', 'sps'); ?></h2>
						<div class="sps-options-content">
							<?php foreach($options as $key => $option) : ?>
								<?php if($key == $size) : ?>
									<button id="sps-showmore"><?php echo '+ ' . __('Show more', 'sps'); ?></button>
									<span id="sps-more">
								<?php endif; ?>
										<label class="sps-options-label"><?php echo $option['option_name']; ?>
											<input type="checkbox">
											<span class="sps-checkbox"></span>
										</label>
								<?php if($key+1 == sizeof($options)) : ?>
									</span>
								<?php endif; ?>
							<?php endforeach; ?>
						</div>
						<button id="sps-showless"><?php echo '- ' . __('Show less', 'sps'); ?></button>
					</div>
				<?php
				}
				?>
				<div class="sps-marks">
					<h2><?php echo __('Marking techniques', 'sps'); ?></h2>
					<div>
						<div class="sps-mark" data-mark="embroidery">
							<div class="mark-image">
								<img src="<?php echo get_site_url() . '/assets/img/OPTIONS/broderie.png'; ?>" alt="Embroidery"/>
							</div>
							<!-- <div class="mark-name">Embroidery</div> -->
						</div>
						<div class="sps-mark" data-mark="embroidery-3d">
							<div class="mark-image">
								<img src="<?php echo get_site_url() . '/assets/img/OPTIONS/sandwich.png'; ?>" alt="Embroidery 3D"/>
							</div>
							<!-- <div class="mark-name">Embroidery 3D</div> -->
						</div>
					</div>
				</div>
			</div>
			<?php
		}
    function sps_popup() { ?>
			<div class="sps-popup">
				<div class="sps-popup-content"></div>
			</div>
			<?php
		}
		function sps_add_comment_to_customization() { ?>
			<div class="sps-add-comment-to-customization">
				<div class="sps-add-comment-to-customization-content">
					<h3>Add comment to customization</h3>
					<div class="sps-add-comment-to-customization-close">
						<i class="fas fa-times"></i>
					</div>
					<textarea class="sps-add-comment-to-customization-textarea" rows="5"></textarea>
					<button class="sps-add-comment-to-customization-button"><?php echo __('Add comment', 'sps'); ?></button>
				</div>
			</div>
			<?php
		}

        function sps_add_comment_to_customization2() { ?>
            <div class="sps-add-comment-to-customization2">
                <div class="sps-add-comment-to-customization-content2">
                    <h3>Specify the delays</h3>
                    <div class="sps-add-comment-to-customization-close2">
                        <i class="fas fa-times"></i>
                    </div>
                    <textarea class="sps-add-comment-to-customization-textarea2" rows="5"></textarea>
                    <button class="sps-add-comment-to-customization-button2"><?php echo __('Add specify', 'sps'); ?></button>
                </div>
            </div>
            <?php
        }
		function sps_quantity_minimum() {
			$quantity_minimum = get_field('quantity_minimum');
			?>
				<div class="sps-quantity-minimum" data-quantity="<?php echo $quantity_minimum; ?>">
					<?php echo __('Quantity minimum:', 'sps') . ' ' . $quantity_minimum ; ?>
				</div>
			<?php
		}
	}
}
?>
