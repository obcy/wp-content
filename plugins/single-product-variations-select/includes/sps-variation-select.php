<?php
class SPS_Variations_Table{
  var $sps_constant = array();
  function __construct($sps_constant = array()) {
    $this->sps_constant = $sps_constant;
    add_action('wp_head',  array(&$this,'wp_head' ));

    $this->add_select_variations_page();

  }
  function add_select_variations_page(){
    include_once("sps-after-single-product-summary.php");
    $objtable = new SPS_After_Single_Product_Summary();
  }
  function wp_head(){
    if (is_product()){
    	wp_register_style('sps-style', plugins_url( '../assets/css/sps-style.css', __FILE__ ));
    	wp_enqueue_style( 'sps-style');

      wp_enqueue_script('sps-script', plugins_url( '../assets/js/sps-script.js', __FILE__));
    }
  }
}
?>
