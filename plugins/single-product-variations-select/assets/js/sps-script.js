jQuery(function($){
  var addToCart = $(".single_add_to_cart_button");
  var quantityInput = $(".cart input[name='quantity']");

  var spsColor = $("#sps-color");
  var sizeS = $("#sps-size-s");
  var sizeM = $("#sps-size-m");
  var sizeL = $("#sps-size-l");
  var sizeXL = $("#sps-size-xl");
  var sizeUniversal = document.getElementById("sps-general-quantity");
  var spsSelect = $(".sps-select");

  var spsButton = $('.sps-dropdown-button');
  var spsButtonCircle = $('.sps-dropdown-button-circle');
  var spsDropdownColors = $('.sps-dropdown-colors');
  var spsDropdownColor = $('.sps-dropdown-circle');
  var color = spsButton.attr('data-color');
  var colorHex = spsDropdownColor.first().attr('data-color-hex');

  var spsMarks = $('.sps-marks');
  var spsMark = $('.sps-mark');
  var mark = '';

  var quantityMinimum = $('.sps-quantity-minimum');
  var quantityMin = quantityMinimum.attr('data-quantity');

  var currentCurrency = $('#sps-current-currency');
  var priceRange = $('#sps-price-range');
  var priceRangeEur = $('.price-range.eur');
  var priceRangeMainEur = $('#price-range-eur');
  var priceRangeUsd = $('.price-range.usd');
  var priceRangeMainUsd = $('#price-range-usd');
  var currency = localStorage.getItem('currency');
  var currencySwitcher = $('#currency-switcher');
  var price = '';

  currencySwitcher.change(function(){
    currency = $(this).val();
    if(currency === 'EUR') {
      price = priceRangeMainEur.text().trim();
      priceRangeUsd.hide();
      priceRangeEur.show();
      priceRange.val(price);
      currentCurrency.val(currency);
    } else {
      price = priceRangeMainUsd.text().trim();
      priceRangeEur.hide();
      priceRangeUsd.show();
      priceRange.val(price);
      currentCurrency.val(currency);
    }
  });
  if(currency === 'EUR') {
    price = priceRangeMainEur.text().trim();
    priceRangeUsd.hide();
    priceRangeEur.show();
    priceRange.val(price);
    currentCurrency.val(currency);
  } else {
    price = priceRangeMainUsd.text().trim();
    priceRangeEur.hide();
    priceRangeUsd.show();
    priceRange.val(price);
    currentCurrency.val(currency);
  }

  //console.log(priceRange, priceRangeEur, priceRangeUsd);

  //Set main quantity 1 after ready
  $(document).ready(function(){
      quantityInput.attr('value', 1);
  });

  spsColor.val(color);
  spsButtonCircle.css('background', colorHex);

  $(window).click(function(){
    spsDropdownColors.hide();
  });
  // Toggle color list
  spsButton.click(function(){
    spsDropdownColors.toggle();
    event.stopPropagation();
  });
  //Set color variables
  spsDropdownColor.click(function(){
    color = $(this).attr("data-color");
    colorHex = $(this).attr("data-color-hex");
    setColor(color, colorHex);
    event.stopPropagation();
  });
  //Set color
  function setColor(color, colorHex) {
    if (color == 'ZCustom') {
      $('#sps-color-picker').trigger('click').change(function(event){
          spsButtonCircle.addClass('custom');
          spsButtonCircle.css('background', event.target.value);
          spsColor.val(event.target.value);
          $("#pa_color").val(color.toLowerCase().replace(' ','-')).trigger('change');
        });
    } else {
      spsButtonCircle.removeClass('custom');
      spsColor.val(color);
      spsButtonCircle.css('background', colorHex);
      spsDropdownColors.toggle();
      $("#pa_color").val(color.toLowerCase().replace(' ','-')).trigger('change');
    }
  }
  //Set default size
  $("#pa_size").val("s");
  //Set default quantity
  $(".sps-quantity").find("input").val(0);
  //Set quantity for sizevar quantityMinimum = $('.sps-quantity-minimum');
  var quantity = quantityMinimum.attr('data-quantity');
  $(".sps-quantity").change(function(){
    var $quantity = $(this).find("input").val();
    switch($(this).attr("data-size")) {
      case "S":
        sizeS.val($quantity);
        break;
      case "M":
        sizeM.val($quantity);
        break;
      case "L":
        sizeL.val($quantity);
        break;
      case "XL":
        sizeXL.val($quantity);
        break;
      case "universal":
        sizeUniversal.value = $quantity;
        break;
    }
  });
  //Set default mark
  // if ( spsMark ) {
  //   $(spsMark[0]).addClass('active');
  //   mark = $(spsMark[0]).attr('data-mark');
  //   $('#sps-mark').val(mark);
  // }
  //Set marking technique
  spsMark.click(function(){

    spsMark.removeClass('active');
    mark = $(this).attr('data-mark');
    $('#sps-mark').val(mark);
    $(this).addClass('active');
  });
  function popup(msg, q, color) {
    var spsPopup = $('.sps-popup'),
        spsPopupContent = $('.sps-popup-content');
    spsPopupContent.html(msg + ' ' + q).css('background', color);
    spsPopup.css('z-index', 10)
      .animate({
        opacity: 1
      });
    setTimeout(function(){
      spsPopup.animate({
        opacity: 0
      },function(){
        spsPopup.css('z-index', -1);
        spsPopupContent.css('z-index', -1);
      });
    }, 3000);
  }
  var spsOptionsShowmore = $('#sps-showmore');
  var spsOptionsShowless = $('#sps-showless');
  var spsMore = $('#sps-more');
  spsMore.fadeOut();
  spsOptionsShowmore.click(function(){
    spsMore.css('opacity', 1 ).fadeIn();
    $(this).hide();
    $(spsOptionsShowless).toggle();
  });
  spsOptionsShowless.click(function(){
    spsMore.css('opacity', 0).fadeOut();
    $(this).hide();
    $(spsOptionsShowmore).toggle();
  });
  //Add to cart event
  //Set quantity for items
  //Set options
  addToCart.click(function() {
    var quantityAll;
    var options = [];
    var spsOptionsCheckedList = $('.sps-options-label input:checkbox:checked');
    quantityInput.attr('min', quantityMin);
    if ( sizeUniversal ) {
      quantityAll = parseInt(sizeUniversal.value);
    } else {
      quantityAll = parseInt(sizeS.val()) + parseInt(sizeM.val()) + parseInt(sizeL.val()) + parseInt(sizeXL.val());
    }
    if(quantityAll < quantityMin){
      quantityInput.attr('value', quantityAll);
      popup('Add a minimum of', quantityMin, 'red');
    } else {
      quantityInput.attr('value', quantityAll);
    }
    if (spsOptionsCheckedList) {
      for (var i = 0; spsOptionsCheckedList.length > i; i++) {
        options[i] = spsOptionsCheckedList[i].labels[0].innerText;
      }
      $('#sps-options').val(options);
    }
  });


  function popupAdd() {
    var spsPopupAdd = $('.sps-add-comment-to-customization'),
        spsPopupAddContent = $('.sps-add-comment-to-customization-content'),
        spsPopupAddClose = $('.sps-add-comment-to-customization-close'),
        spsPopupAddButton = $('.sps-add-comment-to-customization-button');
    spsPopupAdd.css('z-index', 10)
      .animate({
        opacity: 1
      });
    spsPopupAddClose.click(function(){
      spsPopupAdd.animate({
        opacity: 0
      },function(){
        spsPopupAdd.css('z-index', -1);
        spsPopupAddContent.css('z-index', -1);
      });
    });
    spsPopupAddButton.click(function(){
      var spsPopupTextarea = $('.sps-add-comment-to-customization-textarea').val();
      $('#sps-add-comment-to-customization').val(spsPopupTextarea);
      spsPopupAdd.animate({
        opacity: 0
      },function(){
        spsPopupAdd.css('z-index', -1);
        spsPopupAddContent.css('z-index', -1);
      });
    });

  }


  function popupAdd2() {
    console.log("asdas");
    var spsPopupAdd = $('.sps-add-comment-to-customization2'),
        spsPopupAddContent = $('.sps-add-comment-to-customization-content2'),
        spsPopupAddClose = $('.sps-add-comment-to-customization-close2'),
        spsPopupAddButton = $('.sps-add-comment-to-customization-button2');
    spsPopupAdd.css('z-index', 10)
        .animate({
          opacity: 1
        });
    spsPopupAddClose.click(function(){
      spsPopupAdd.animate({
        opacity: 0
      },function(){
        spsPopupAdd.css('z-index', -1);
        spsPopupAddContent.css('z-index', -1);
      });
    });
    spsPopupAddButton.click(function(){
      var spsPopupTextarea = $('.sps-add-comment-to-customization-textarea2').val();
      $('#sps-add-comment-to-customization2').val(spsPopupTextarea);
      spsPopupAdd.animate({
        opacity: 0
      },function(){
        spsPopupAdd.css('z-index', -1);
        spsPopupAddContent.css('z-index', -1);
      });
    });



  }


  $('.specify-the-delays button').click(function(){
    popupAdd2();
  });
  var buttomCustomizing = $('#fpd-start-customizing-button');
  buttomCustomizing.after('<div id="add-comment-to-customization" data-toggle="tooltip" data-placement="left" title="Add comment to customization"><i class="fas fa-pen"></i></div>');
  $('#add-comment-to-customization').click(function(){
    popupAdd();
  });
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
  var customDescription = $('#sps-custom-description'),
      shortDescription = $('.woocommerce-product-details__short-description p').text();

  customDescription.val(shortDescription);
  ($('#fpd-start-customizing-button').length == 0) && $('.single_add_to_cart_button').css('width', '100%');
});
